DEPLOYMENT_ENVIRONMENT=${1:-dev}
COMPOSE_FILE_NAME="stack-${DEPLOYMENT_ENVIRONMENT}.yml"
echo "environment: ${DEPLOYMENT_ENVIRONMENT}"
docker login registry.gitlab.com
docker stack deploy \
    --with-registry-auth \
    --compose-file ${COMPOSE_FILE_NAME} \
    --resolve-image always \
    --prune \
    innometeo-data-processing-system
