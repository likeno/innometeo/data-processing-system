#!/usr/bin/env python3
"""
Entrypoint script for the docker image

This script creates the .netrc file and the .ssh config dir. After
that, it calls the original ``docker-entrypoint`` script from the
base docker image.
"""

import argparse
import os
import re
import shlex
import shutil
import subprocess
import time
from functools import partial
from pathlib import Path

import redis
from airflow import configuration
from airflow.bin.cli import CLIFactory
from airflow.contrib.auth.backends.password_auth import PasswordUser
from airflow import models
from airflow import settings
from sqlalchemy import create_engine
from sqlalchemy.exc import OperationalError
from sqlalchemy.orm.exc import NoResultFound

print = partial(print, flush=True)

DB_INIT_WAITING_SECONDS = 10
SECRETS_DIRECTORY = Path("/run/secrets")
SSH_SECRETS_PATTERN = "^ssh-"
NETRC_SECRET_PATTERN = "^netrc"
SECRET_TO_ENVIRONMENT_EXCLUDE_PATTERNS = [
    SSH_SECRETS_PATTERN,
    NETRC_SECRET_PATTERN,
]


def copy_ssh_config(secrets_directory: Path, pattern="ssh"):
    """Put ssh-related config and keys passed as a docker secrets in place."""
    for item in secrets_directory.iterdir():
        matches_pattern = bool(re.search(pattern, item.name))
        if item.is_file() and matches_pattern:
            target_name = re.sub(pattern, "", item.name)
            target_dir = Path("~/.ssh").expanduser()
            target_dir.mkdir(exist_ok=True)
            destination = target_dir / target_name
            shutil.copyfile(str(item), str(destination))


def copy_netrc_config(secrets_directory: Path, pattern="netrc"):
    """Put netrc file passed as docker secret into the expected place"""
    for item in secrets_directory.iterdir():
        matches_pattern = bool(re.search(pattern, item.name))
        if item.is_file() and matches_pattern:
            destination = Path("~/.netrc").expanduser()
            shutil.copyfile(str(item), str(destination))
            destination.chmod(0o600)


def create_environment(secrets_directory: Path, exclude_patterns=None):
    """Put any docker secrets in the environment at runtime"""
    patterns = list(exclude_patterns) if exclude_patterns is not None else []
    for item in secrets_directory.iterdir():
        for patt in patterns:
            if re.search(patt, item.name) is not None:
                break
        else:
            if item.is_file():
                contents = item.read_text()
                os.environ[item.name] = contents.strip()


def generate_user(username, password):
    """Generate an airflow user that is able to login to the web UI"""
    session = settings.Session()
    try:
        session.query(models.User).filter(
            models.User.username == username).one()
    except NoResultFound:
        user = PasswordUser(models.User())
        user.username = username
        user.password = password
        session.add(user)
        session.commit()
        session.close()


def run_scheduler(args, remaining_args):
    handle_database_setup()
    handle_executor_setup()
    launch_airflow(["scheduler"] + remaining_args)


def run_webserver(args, remaining_args):
    handle_database_setup(initdb=True)
    generate_user("admin", os.environ.get("AIRFLOW_ADMIN_PASSWORD", "admin"))
    handle_executor_setup()
    launch_airflow(["webserver"] + remaining_args)


def run_worker(args, remaining_args):
    print("about to run handle_database_setup")
    handle_database_setup()
    print("about to run handle_executor_setup")
    handle_executor_setup()
    if SECRETS_DIRECTORY.is_dir():
        print("about to run copy_netrc_config")
        copy_netrc_config(SECRETS_DIRECTORY, NETRC_SECRET_PATTERN)
        print("about to run copy_ssh_config")
        copy_ssh_config(SECRETS_DIRECTORY, SSH_SECRETS_PATTERN)
    print("about delegate to airflow cli")
    print("BROKER_URL", configuration.conf.get("celery", "BROKER_URL"))
    print("RESULT_BACKEND", configuration.conf.get("celery", "RESULT_BACKEND"))
    print("sqlalchemy_conn", configuration.conf.get("core", "sql_alchemy_conn"))
    print("airflow_home via conf", configuration.conf.get("core", "airflow_home"))
    print("airflow_home via settings", settings.AIRFLOW_HOME)
    launch_airflow(["worker"] + remaining_args)


def handle_executor_setup():
    if "celery" in configuration.conf.get("core", "executor").lower():
        wait_for_redis()


def launch_airflow(args):
    airflow_parser = CLIFactory.get_parser()
    airflow_args = airflow_parser.parse_args(args)
    airflow_args.func(airflow_args)


def handle_database_setup(initdb=False):
    db_config_value = get_db_url()
    if "postgres" in db_config_value:
        wait_for_postgres()
    if initdb:
        launch_airflow(["initdb"])
    else:
        time.sleep(DB_INIT_WAITING_SECONDS)  # await db init by ``web`` container


def get_db_url():
    config_section = configuration.conf.getsection("core")
    connection_config_command = config_section.get("sql_alchemy_conn_cmd")
    if connection_config_command is not None:
        result = configuration.run_command(connection_config_command)
    else:
        result = configuration.conf.get("core", "sql_alchemy_conn")
    return result


def wait_for_postgres(max_tries=10, wait_seconds=3):
    database_url = get_db_url()
    engine = create_engine(database_url)
    current_try = 0
    while current_try < max_tries:
        try:
            engine.execute("SELECT version();")
            print("Database is already up!")
            break
        except OperationalError:
            print("Database not responding yet ...")
            current_try += 1
            time.sleep(wait_seconds)
    else:
        raise RuntimeError(
            "Database not responding at {} after {} tries. "
            "Giving up".format(database_url, max_tries)
        )


def get_redis_url():
    config_section = configuration.conf.getsection("celery")
    connection_config_command = config_section.get("result_backend_cmd")
    if connection_config_command is not None:
        result = configuration.run_command(connection_config_command)
    else:
        result = configuration.conf.get("celery", "result_backend")
    return result


def wait_for_redis(max_tries: int = 10, wait_seconds: int = 3):
    redis_url = get_redis_url()
    current_try = 0
    while current_try < max_tries:
        redis_connection = redis.Redis.from_url(redis_url)
        try:
            redis_connection.get("dummy")
            break
        except redis.ConnectionError:
            print("Redis not responding yet ...")
            current_try += 1
            time.sleep(wait_seconds)
    else:
        raise RuntimeError(
            "Redis not responding at {} after {} tries. "
            "Giving up".format(redis_url, max_tries)
        )


def _get_parser():
    parser = argparse.ArgumentParser()
    sub_parsers = parser.add_subparsers()
    for sub_command in ["webserver", "worker", "scheduler"]:
        sub_parser = sub_parsers.add_parser(sub_command)
        func = globals()["run_{}".format(sub_command)]
        sub_parser.set_defaults(func=func)
    return parser


if __name__ == '__main__':
    parser = _get_parser()
    args, remaining_args = parser.parse_known_args()
    if SECRETS_DIRECTORY.is_dir():
        create_environment(
            SECRETS_DIRECTORY, SECRET_TO_ENVIRONMENT_EXCLUDE_PATTERNS)
    print(f"After loading the secrets into environment")
    print(f"os.environ: {os.environ}")
    print("about to leave the main block")
    args.func(args, remaining_args)
