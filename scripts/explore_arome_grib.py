"""A script to explore AROME GRIB data."""

import argparse
import datetime as dt

from osgeo import gdal


def main(file_path):
    ds = gdal.Open(file_path)
    slot = dt.datetime.strptime(file_path.rpartition("_")[-1], "%Y%m%d%H")
    bands = {}
    for band_number in range(1, ds.RasterCount+1):
        band = ds.GetRasterBand(band_number)
        metadata = band.GetMetadata()
        short_name = metadata["GRIB_SHORT_NAME"]
        forecast_seconds = int(
            metadata["GRIB_FORECAST_SECONDS"].strip().split(" ")[0])
        forecast_slot = slot + dt.timedelta(seconds=forecast_seconds)
        bands.setdefault(short_name, [])
        bands[short_name].append(
            (
                band_number, 
                forecast_slot.strftime("%Y-%m-%d %H:%M:%S"), 
                metadata["GRIB_COMMENT"], 
                band.GetDescription(),
                metadata["GRIB_ELEMENT"],
            )
        )
    return bands


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "path",
        help="Full path to the AROME file to explore"
    )
    args = parser.parse_args()
    found_bands = main(args.path)
    print("bands:")
    all_forecasts = set()
    for name, info in found_bands.items():
        print("\t{}".format(name))
        sorted_info = sorted(info, key=lambda i: i[1])
        for num, forecast, comment, description, element in sorted_info:
            print("\t\t{} - {} - {} - {} - {}".format(num, forecast,
                                                      comment, description, 
                                                      element))
            all_forecasts.add(forecast)
    print("------------------------")
    print("{} forecasts".format(len(all_forecasts)))
    for item in sorted(list(all_forecasts)):
        print(item)
