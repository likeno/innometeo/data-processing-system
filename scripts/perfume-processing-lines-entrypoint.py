#!/usr/bin/env python3
"""An entrypoint script for the docker image.

This script creates the .netrc file and .ssh config dir.
After that it calls the original ``docker-entrypoint`` script from the
base docker image.`

"""

import argparse
import logging
import os
import shutil
from subprocess import call
import pwd

logger = logging.getLogger(__name__)

USER_NAME = 'likeno'

# Get user and group ids for likeno user
likeno_pwnam = pwd.getpwnam(USER_NAME)
LIKENO_UID = likeno_pwnam.pw_uid
LIKENO_GID = likeno_pwnam.pw_gid


def copy_ssh_config():
    """ The docker host .ssh must be a shared
    volume pointing to the container's path: /tmp/.ssh
    """
    tmp_ssh_path = '/tmp/.ssh'
    ssh_path = '/home/{}/.ssh'.format(USER_NAME)

    if os.path.isdir(tmp_ssh_path): 
        if os.path.isdir(ssh_path):
            shutil.rmtree(ssh_path)
        shutil.copytree(tmp_ssh_path, ssh_path)
        os.chown(ssh_path, LIKENO_UID, LIKENO_GID)
        for root_path, dirs, files in os.walk(ssh_path):
            for path in [os.path.join(root_path, dir_name) for dir_name in dirs]:
                os.chown(path, LIKENO_UID, LIKENO_GID)
            for path in [os.path.join(root_path, file_name) for file_name in files]:
                os.chown(path, LIKENO_UID, LIKENO_GID)


def copy_netrc_config():
    """ The docker host .netrc must be a shared
    volume pointing to the container's path: /tmp/.netrc
    """
    tmp_netrc_path = '/tmp/.netrc'
    netrc_path = '/home/{}/.netrc'.format(USER_NAME)

    if os.path.isfile(tmp_netrc_path): 
        if os.path.isfile(netrc_path):
            os.remove(netrc_path)
        shutil.copy(tmp_netrc_path, netrc_path)
        os.chown(netrc_path, LIKENO_UID, LIKENO_GID)


def call_original_entrypoint(entrypoint_args):
    """Run the original entrypoint as 'likeno' user.
    """
    original_entrypoint_command = (
        'exec su {} -c "docker-entrypoint {}"'.format(
            USER_NAME, ' '.join(entrypoint_args)
        )
    )
    logger.debug("system call: {}".format(original_entrypoint_command))
    call(original_entrypoint_command, shell=True)


def main(entrypoint_args):
    if (len(entrypoint_args) == 0 or
        entrypoint_args[0] == 'worker'):
        copy_netrc_config()
        copy_ssh_config()
    if len(entrypoint_args) != 0:
        call_original_entrypoint(entrypoint_args)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    args, remaining_args = parser.parse_known_args()
    main(remaining_args)
