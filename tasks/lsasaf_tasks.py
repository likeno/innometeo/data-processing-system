# MIT License
#
# Copyright (c) 2020 likeno
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""soko callback functions for the configured packages."""

import bz2
import datetime as dt
import logging
import os
import shutil
import typing
from urllib.parse import urlparse

import xarray as xr
import numpy as np

import nc_juice
from msg2nc.msg2nc import convert_h5_to_nc
from soko import packages

from processing import frpPixel2shapefile
from processing import lsasafdset2geotiff

logger = logging.getLogger(__name__)


def get_basic(
        retrieved_inputs: typing.Iterable[packages.RetrievedMember],
        package: packages.PythonCallbackPackage,
        output_dir: str,
        decompress: typing.Optional[bool] = True,
        **kwargs
) -> typing.Tuple[
    typing.List[str],
    typing.List[str],
    packages.PythonCallbackPackage
]:
    """Return retrieved input paths, output paths and soko package."""
    input_paths = []
    for member in retrieved_inputs:
        if member.local_path is not None:
            retrieved = _retrieve_lsasaf_input(
                member.local_path,
                working_dir=output_dir,
                decompress=decompress
            )
            input_paths.append(retrieved)
    output_paths = []
    for output_resource in package.outputs:
        path = os.path.join(
            output_dir,
            os.path.basename(urlparse(output_resource.search_names[0]).path)
        )
        output_paths.append(path)
    return input_paths, output_paths, package


def retrieve_frm_inputs(
        *retrieved_inputs,
        package: packages.PythonCallbackPackage,
        output_dir: str,
        timeslot: dt.datetime,
        **kwargs
):
    input_paths, (output_path,), package = get_basic(
        retrieved_inputs, package, output_dir, **kwargs)
    temp_output_paths = []
    for input_path in input_paths:
        fname = f"{os.path.basename(input_path)}.nc"
        out_path = os.path.join(output_dir, fname)
        convert_h5_to_nc(input_path, out_path, with_time="")
        temp_output_paths.append(out_path)
    merge_frm(
        temp_output_paths,
        output_path,
        ref_time=dt.datetime(timeslot.year, timeslot.month, timeslot.day, 12)
    )
    for tmp_output_path in temp_output_paths:
        os.remove(tmp_output_path)


def merge_frm(
        input_paths: typing.List[str],
        output_path: str,
        ref_time: typing.Optional[dt.datetime]=None
):
    logger.info(f"Merging LSASAF FRM files: {input_paths}")
    step_dim_attrs = {
        'long_name': 'time since forecast_reference_time',
        'standard_name':  'forecast_period'
    }
    time_dim_attrs = {
        'long_name': 'initial time of forecast',
        'standard_name':  'forecast_reference_time'
    }
    xdset = xr.open_mfdataset(
        input_paths,
        cache=False,
        combine='by_coords',
        concat_dim='time'
    )
    if ref_time is None:
        ref_time = dt.datetime.strptime(
            xr.coding.times.encode_cf_datetime(xdset.time)[1],
            'days since %Y-%m-%d %H:%M:%S'
        )

    logger.debug(f"ref_time: {ref_time}")
    t0 = np.datetime64(ref_time)
    xdset.time.data = xdset.time.data[:] - t0
    xdset = xdset.rename_dims({'time': 'step'}).rename_vars({'time': 'step'})
    xdset = xdset.expand_dims({'time': [t0]})
    xdset.step.attrs.update(step_dim_attrs)
    xdset.time.attrs.update(time_dim_attrs)
    xdset.time.attrs['axis'] = xdset.step.attrs.pop('axis')
    grid_mapping_var, = [name for name, value in xdset.data_vars.items()
                         if 'grid_mapping_name' in  value.attrs.keys()]
    xdset[grid_mapping_var] = xr.DataArray(
        '',
        name=grid_mapping_var,
        dims=(),
        attrs=xdset[grid_mapping_var].attrs
    )
    logger.info(f"Write netCDF output file: {output_path}")
    xdset.to_netcdf(
        output_path,
        'w',
        'NETCDF4',
        encoding={name:{'zlib': True, 'complevel': 9} for name in xdset.data_vars.keys() if name != grid_mapping_var}
    )


def transform_netcdf(*retrieved_inputs, **kwargs):

    (input_paths, (output_path,),
     package) = get_basic(retrieved_inputs, **kwargs)

    if any(input_paths):
        nc_juice.process_nc_datasets(
            input_paths, output_path,
            **package.callback_arguments
        )
    else:
        logger.debug("Missing input files for: {}".format(
            [retrieved_member.member
             for retrieved_member in retrieved_inputs]
        ))


def extract_lsasaf_dataset_to_netcdf(*frm_inputs, **kwargs):
    input_path_list = [retrieved_member.local_path
                       for retrieved_member in frm_inputs]
    package = kwargs["package"]
    output_path = urlparse(package.outputs[0].search_urls[0]).path
    if any(input_path_list):
        lsasafdset2geotiff.extract_to_netcdf(
            input_path_list, output_path, kwargs["dataset_id"],
            kwargs.get("bounding_box", None),
            kwargs.get("raw_scale_factor", False)
        )
    else:
        logger.debug("Missing any input files of: {}".format(
            [retrieved_member.member
             for retrieved_member in frm_inputs]
        ))


def retrieve_frp_input(*retrieved_inputs, **kwargs):
    (input_paths, output_paths,
     package) = get_basic(retrieved_inputs, **kwargs)


def extract_frp_data(retrieved_frp, **kwargs):
    package = kwargs["package"]
    jinja_env = package.jinja_environment
    template = jinja_env.from_string(kwargs["output_prefix"])
    # timeslot is a dynamic property of package so we cannot rely on
    # __dict__ alone
    render_context = dict(package.__dict__)
    render_context.update({"timeslot": package.timeslot})
    output_prefix = template.render(**render_context)
    input_path = retrieved_frp.local_path
    output_path_prefix = os.path.join(kwargs['output_dir'],
                                      output_prefix)
    logger.debug("Extracting FRP data from {!r}".format(input_path))
    frpPixel2shapefile.main(
        input_path, output_path_prefix, kwargs["dataset_id"])


def _retrieve_lsasaf_input(
        local_path: str,
        working_dir: str,
        decompress: typing.Optional[bool] = True,
        copy: typing.Optional[bool] = False
) -> str:
    if local_path.endswith(".bz2") and decompress:
        input_path = os.path.join(
            working_dir,
            os.path.basename(local_path).replace(".bz2", "")
        )
        logger.debug(
            "Decompressing {!r} to {!r}...".format(local_path, input_path))
        with bz2.BZ2File(local_path) as source_fh, \
                open(input_path, "wb") as destination_fh:
            destination_fh.write(source_fh.read())
    elif copy:
        shutil.copy(local_path, working_dir)
        input_path = os.path.join(
            working_dir, os.path.basename(local_path))
    else:
        input_path = local_path
    return input_path


