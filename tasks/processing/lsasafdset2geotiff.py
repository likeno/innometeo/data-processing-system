#!/usr/bin/env python
# -*- coding: utf8 -*-

# MIT License
# 
# Copyright (c) 2017 likeno
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""
gdalwarp \
    -of GTiff \
    -s_srs '+proj=geos +lon_0=0 +h=35785831 +x_0=0 +y_0=0 +ellps=WGS84 \
            +units=m +no_defs ' \
    -t_srs '+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 \
            +y_0=0 +k=1.0 +units=m +nadgrids=@null +wktext +no_defs' \
    -srcnodata -8000 \
    -overwrite \
    -te -1424889.482153902 4028802.0261344104 \
        -133583.38895192833 5590090.3415273065 \
    -r bilinear \
    'HDF5:"data/HDF5_LSASAF_MSG_FRM-F024_MSG-Disk_201708061200"://FWI' \
    test_out2.tif

"""
import os
import sys
import datetime as dt
import logging
from collections import OrderedDict
import numpy as np
import h5py
from osgeo import gdal
from osgeo import ogr
from osgeo import osr

logger = logging.getLogger(__name__)
gdal.UseExceptions()

# xmin, ymin, xmax ymax
DEFAULT_BOUNDING_BOX = [-12.80, 34.00, -1.20, 44.80]

def extract_to_netcdf(input_path_list, output_path, #output_dir,
                      dset_name='FWI', bbox=None,
                      raw_scale_factor=True, crop_srs_code=4326,
                      trg_srs_code=3857):

    for time_idx, input_path in enumerate(sorted(input_path_list)):

        logger.debug("Open hdf5 file {}...".format(input_path))
        src_subdataset = open_lsasaf_hdf5(input_path, dset_name)
        mem_subdataset = process_dataset(src_subdataset, bbox,
                                         raw_scale_factor, crop_srs_code,
                                         trg_srs_code)

        time_step = int(src_subdataset.GetMetadataItem('TIME_RANGE').split('-')[1])

        if time_idx == 0:
            ref_time = dt.datetime.strptime(
                src_subdataset.GetMetadataItem('IMAGE_ACQUISITION_TIME'),
                "%Y%m%d%H%M%S"
            ) - dt.timedelta(hours = time_step)

            size_t = len(input_path_list)
            #dims_settings_dict = OrderedDict([
            #    ('runtime', {'size': 1, 'attrs': {
            #        'NAME': "runtime",
            #        'standard_name': "forecast_reference_time",
            #        'long_name': "run time of the model",
            #        'axis': "RunTime",
            #        'units': ref_time.strftime("days since %Y-%m-%d %H:%M:00")
            #    }}),
            #    ('time', {'size': size_t, 'attrs': {
            #        'NAME': "time",
            #        'long_name': "forecast valid time",
            #        'axis': "Time",
            #        'units': "hours since forecast reference time (runtime)"
            #    }}),
            #])

            dims_settings_dict = OrderedDict([
                ('time', {'size': size_t, 'attrs': {
                    'NAME': np.array("time",'|S'),
                    'standard_name': np.array("time",'|S'),
                    'long_name': np.array("time",'|S'),
                    'axis': np.array("T",'|S'),
                    'units': np.array(ref_time.strftime("hours since %Y-%m-%d %H:%M:00"),'|S'),
                }}),
            ])

            nc_fid, dims = create_netcdf_base(output_path, mem_subdataset,
                                              dims_settings_dict)

        update_netcdf(nc_fid, dims, dset_name, mem_subdataset,
                      time_idx, time_step)


        src_subdataset, mem_subdataset = (None, None)
        tmp_aux = input_path + '.aux.xml'
        if os.path.isfile(tmp_aux):
            logger.debug("Removing temporary file: {}...".format(tmp_aux))
            os.remove(tmp_aux)


def process_dataset(src_subdataset, bbox=None,
                    raw_scale_factor=True, crop_srs_code=4326,
                    trg_srs_code=3857):
    bbox = list(bbox) if bbox is not None else DEFAULT_BOUNDING_BOX
    croped_subdataset = crop_data(src_subdataset, bbox, crop_srs_code)
    trg_srs = osr.SpatialReference()
    trg_srs.ImportFromEPSG(trg_srs_code)
    reproj_subdataset = gdal.AutoCreateWarpedVRT(
        croped_subdataset, None, trg_srs.ExportToWkt(),
        gdal.GRA_NearestNeighbour, 0.125
    )
    recroped_subdataset = crop_data(reproj_subdataset, bbox,
                                    crop_srs_code)
    if raw_scale_factor:
        gdal_driver = gdal.GetDriverByName('MEM')
        src_band = recroped_subdataset.GetRasterBand(1)
        mem_subdataset = gdal_driver.Create(
            '', src_band.XSize, src_band.YSize,
            1, gdal.GDT_Float32
        )
        src_projection = recroped_subdataset.GetProjection()
        src_geotransform = recroped_subdataset.GetGeoTransform()
        mem_subdataset.SetProjection(src_projection)
        mem_subdataset.SetGeoTransform(src_geotransform)
        trg_band = mem_subdataset.GetRasterBand(1)
        fill_value = src_band.GetNoDataValue()
        trg_band.SetNoDataValue(fill_value)
        data = np.array(src_band.ReadAsArray(), np.float32)
        fill_val_mask = (data == fill_value)
        data[~fill_val_mask] = (data[~fill_val_mask] * src_band.GetScale() +
                                src_band.GetOffset())
        trg_band.WriteArray(data)
        gdal_driver = None
    else:
        mem_subdataset = recroped_subdataset
    return mem_subdataset


def create_netcdf_base(output_path, mem_subdataset, dims_settings_dict):
    logger.debug("Create netCDF file: {}...".format(output_path))
    nc_driver = gdal.GetDriverByName('netCDF')
    if os.path.isfile(output_path):
        os.remove(output_path)
    base_copy = nc_driver.CreateCopy(
        output_path, mem_subdataset,
        options=['FORMAT=NC4','COMPRESS=DEFLATE','ZLEVEL=9']
    )
    base_copy.FlushCache()
    base_copy, nc_driver = (None, None)
    nc_fid, dims = _reset_netcdf_dimensions(output_path, dims_settings_dict)
    return nc_fid, dims



def update_netcdf(nc_fid, dims, dset_name, mem_subdataset,
                  time_idx, time_step):

    raster_band = mem_subdataset.GetRasterBand(1)
    data = raster_band.ReadAsArray()
    nc_var = nc_fid.get(dset_name)
    if nc_var is None:
        datatype = data.dtype
        var_shape = [dim.size for dim in dims]
        nc_var = nc_fid.create_dataset(
            dset_name, var_shape, datatype,
            compression="gzip", compression_opts=9,# shuffle=True,
            fillvalue=raster_band.GetNoDataValue()
        )

        grid_mapping_name, = [k for k,v in nc_fid.items()
                              if 'grid_mapping_name' in v.attrs.keys()]
        nc_var.attrs.create('grid_mapping', np.array(grid_mapping_name,'|S'))

        for dim_idx, dim in enumerate(dims):
            nc_var.dims[dim_idx].attach_scale(dim)
        
        map_atts = {
            #'units': mem_band.GetMetadataItem('GRIB_UNIT'),
            #'long_name': mem_band.GetMetadataItem('GRIB_COMMENT'),
            'units': np.array(mem_subdataset.GetMetadataItem('{}_UNITS'.format(dset_name)),'|S'),
            '_FillValue': raster_band.GetNoDataValue(),
            'scale_factor': (raster_band.GetScale()
                             if raster_band.GetScale() != 1.
                             else None),
            'add_offset': (raster_band.GetOffset()
                             if raster_band.GetOffset() != 0
                             else None),
        }
        for key, val in map_atts.items():
            if (val is not None and
                nc_var.attrs.get(key) is None):
                nc_var.attrs.create(key, val)

    #nc_var[0, time_idx, :, :] = data
    nc_var[time_idx, :, :] = data
    nc_fid['time'][time_idx] = time_step

    nc_fid.flush()


def _reset_netcdf_dimensions(filename, dims_orddict):
    gdal_ds = gdal.Open('NETCDF:"{}":Band1'.format(filename))
    metadata = gdal_ds.GetMetadata()
    nc_fid = h5py.File(filename, 'r+')
    (dim_y_name, dim_y),  = nc_fid['Band1'].dims[0].items()
    (dim_x_name, dim_x),  = nc_fid['Band1'].dims[1].items()
    grid_mapping_name, = [k for k,v in nc_fid.items()
                          if 'grid_mapping_name' in v.attrs.keys()]
    # Reset 'bad' format attribute values
    for gdal_attr_name, gdal_attr_value in metadata.items():
        var_name, split_char, attr_name =  gdal_attr_name.partition('#')
        if var_name == 'NC_GLOBAL':
            var_name = '/'
        if var_name in (dim_y_name, dim_x_name, grid_mapping_name, '/'):
            try:
                attr_val = nc_fid[var_name].attrs[attr_name]
            except IOError:
                try:
                    nc_fid[var_name].attrs[attr_name] = gdal_attr_value
                except RuntimeError:
                    pass
    nc_fid['Band1'].dims[0].detach_scale(dim_y)
    nc_fid['Band1'].dims[1].detach_scale(dim_x)
    nc_fid.pop('Band1');
    for key in nc_fid.attrs.keys():
        if key in ('history', 'GDAL'):
            nc_fid.attrs.__delitem__(key)
    dims_list = []
    for dim_idx, (dim_name,
                  dim_props) in enumerate(dims_orddict.items()):
        dim_size = dim_props['size']
        dim_attrs = dim_props['attrs']
        dim = nc_fid.create_dataset(dim_name, (dim_size,), np.int32,
                                    range(dim_size))
        dim.attrs.create('_Netcdf4Dimid', np.array(dim_idx, np.int32))
        for attr_name, attr_value in dim_attrs.items():
            dim.attrs.create(attr_name, attr_value)
        dims_list.append(dim)
    dim_y[:] = np.flipud(dim_y[:]) # Flip up-down y axis...
    dim_y.attrs.create('axis', np.array("Y",'|S'))
    dim_y.attrs['_Netcdf4Dimid'] = len(dims_list)
    dim_x.attrs.create('axis', np.array("X",'|S'))
    dim_x.attrs['_Netcdf4Dimid'] = len(dims_list) + 1
    dims_list.extend([dim_y, dim_x])
    return nc_fid, dims_list


def main(input_path, output_dir, dset_name='FWI', bbox=None,
         raw_scale_factor=True, crop_srs_code=4326, trg_srs_code=3857,
         driver_name = "GTiff"):
     
    logger.debug("Open hdf5 file {}...".format(input_path))
    src_subdataset = open_lsasaf_hdf5(input_path, dset_name)
    mem_subdataset = process_dataset(src_subdataset, bbox,
                                     raw_scale_factor, crop_srs_code,
                                     trg_srs_code)

    gdal_driver = gdal.GetDriverByName(driver_name)

    output_path = os.path.join(
        output_dir,
        os.path.basename(input_path).replace("HDF5_", "").replace(
            src_subdataset.GetMetadata()["PRODUCT"], dset_name) + ".tif"
    )

    trg_subdataset = gdal_driver.CreateCopy(output_path, mem_subdataset)
    trg_subdataset.FlushCache()
    src_subdataset, mem_subdataset, trg_subdataset = (None, None, None)
    gdal_driver = None
    tmp_aux = input_path + '.aux.xml'
    if os.path.isfile(tmp_aux):
        os.remove(tmp_aux)


def open_lsasaf_hdf5(input_path, dataset_name):
    src_subdataset = gdal.Open(
        'HDF5:"{}"://{}'.format(input_path,
                                dataset_name),
        gdal.GA_ReadOnly
    )
    src_srs = osr.SpatialReference()
    src_srs.ImportFromProj4('+proj=geos +h=35785831')
    src_subdataset.SetProjection(src_srs.ExportToWkt())
    src_subdataset.SetGeoTransform(get_lsasaf_geot(
        int(src_subdataset.GetMetadata()['LOFF']),
        int(src_subdataset.GetMetadata()['COFF'])
    ))

    src_raster_band = src_subdataset.GetRasterBand(1)
    src_raster_band.SetScale(
        1. / float(src_raster_band.GetMetadata()[
            '{}_SCALING_FACTOR'.format(dataset_name)
        ])
    )
    src_raster_band.SetNoDataValue(
        int(src_raster_band.GetMetadata()[
            '{}_MISSING_VALUE'.format(dataset_name)
        ])
    )
    return src_subdataset


def crop_data(src_subdataset, bounding_box, crop_srs_code=4326,
              border_pad=(0, 0)):
    crop_srs = osr.SpatialReference()
    if type(crop_srs_code) == int:
        crop_srs.ImportFromEPSG(crop_srs_code)
    else:
        crop_srs.ImportFromProj4(crop_srs_code)

    src_srs = osr.SpatialReference()
    src_srs.ImportFromWkt(src_subdataset.GetProjection())
    src_geo_t = src_subdataset.GetGeoTransform()

    transform = osr.CoordinateTransformation(crop_srs, src_srs)
    point = ogr.Geometry(ogr.wkbPoint)

    point.AddPoint(bounding_box[0] - border_pad[0],
                   bounding_box[1] - border_pad[1])
    point.Transform(transform)
    ll_xy = point.GetPoint()[:-1]

    point.AddPoint(bounding_box[2] + border_pad[0],
                   bounding_box[3] + border_pad[1])
    point.Transform(transform)
    ur_xy = point.GetPoint()[:-1]

    (xmin, ymin), (xmax, ymax) = ll_xy, ur_xy

    col_i = np.int(np.round((xmin - src_geo_t[0]) / src_geo_t[1]))
    col_f = np.int(np.round((xmax - src_geo_t[0]) / src_geo_t[1]))
    lin_i = np.int(np.round((ymax - src_geo_t[3]) / src_geo_t[5]))
    lin_f = np.int(np.round((ymin - src_geo_t[3]) / src_geo_t[5]))

    src_raster_band = src_subdataset.GetRasterBand(1)
    croped_geo_t = (xmin, src_geo_t[1], 0, ymax, 0, src_geo_t[5])
    data = src_raster_band.ReadAsArray()[lin_i:lin_f, col_i:col_f]

    mem_driver = gdal.GetDriverByName("MEM")
    croped_subdataset = mem_driver.Create(
        "", data.shape[1], data.shape[0], 1, gdal.GDT_Int16)
    croped_subdataset.SetGeoTransform(croped_geo_t)
    croped_subdataset.SetProjection(src_srs.ExportToWkt())
    croped_raster_band = croped_subdataset.GetRasterBand(1)
    croped_raster_band.WriteArray(data)
    croped_raster_band.SetScale(src_raster_band.GetScale())
    croped_raster_band.SetNoDataValue(src_raster_band.GetNoDataValue())
    return croped_subdataset


def get_lsasaf_geot(loff=1857, coff=1857):
    """Return MSG Geostationary projection geotransform for the given area

    [top left x, w-e pixel resolution, 0,
     top left y, 0, n-s pixel resolution (negative value)]

    """
    # LSASAF HDF5 geostationary projection
    # columns and lines resolution:
    col_res, lin_res = (3000.403165817260742,
                        3000.403165817260742)
    return (
        -1 * coff * col_res,
        col_res,
        0,
        loff * lin_res,
        0,
        -1 * lin_res
    )


if __name__ == "__main__":
    main(*sys.argv[1:])
