#!/usr/bin/env python
# -*- coding: utf8 -*-

# MIT License
#
# Copyright (c) 2017 likeno
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from .grib_juicer import *
from .grib_juicer import _create_netcdf_base
from .grib_juicer import _reset_netcdf_multidim
from .grib_juicer import _get_output_path
from PIL import Image
import numpy as np
import sys

logger = logging.getLogger(__name__)

DEFAULT_COMPONENTS_FILENAME_TEMPLATE = (
    "ECMWF_OPER_001_FC_SP_ATP_090_10UV-TRC_"
    "{{ REF_TIME | format('%Y%m%d%H') }}_{{ FORECAST_TIME_STEP }}.png"
)

DEFAULT_MODULE_FILENAME_TEMPLATE = (
    "ECMWF_OPER_001_FC_SP_ATP_090_10UV-MOD_"
    "{{ REF_TIME | format('%Y%m%d%H') }}_{{ FORECAST_TIME_STEP }}.tiff"
)

def extract_wind_speed(*args, **kwargs):
    if (kwargs.get('extraction_method') is not None and
        'multilayer' in kwargs.get('extraction_method')):
        if kwargs.get('driver_name') is not None:
            kwargs.pop('driver_name')
        extract_wind_speed_to_multilayer_netcdf(*args, **kwargs)
    else:
        extract_wind_speed_to_singlelayer_files(*args, **kwargs)

def extract_wind_speed_to_singlelayer_files(
    grib_path, output_dir, output_filename_template=None,
    u_filter=('GRIB_ELEMENT','10U'), v_filter=('GRIB_ELEMENT','10V'),
    common_filter={'GRIB_SHORT_NAME':'0-SFC'},
    couple_keyword='GRIB_FORECAST_SECONDS',
    epsg_code=None, scale_factor=1., resample_factor=1.,
    extraction_method='singlelayer', step_ge=0, step_le=72,
    driver_name='WebGL'):
    compute_module = (extraction_method == "compute_module")
    filter_rules = common_filter.copy()
    filter_rules.update({u_filter[0]:[u_filter[1]]})
    filter_rules[v_filter[0]].append(v_filter[1])
    output_filename_template = (
        output_filename_template or (
            DEFAULT_MODULE_FILENAME_TEMPLATE
            if compute_module else DEFAULT_COMPONENTS_FILENAME_TEMPLATE
        )
    )
    try:
        grib_ds = gdal.Open(grib_path, gdal.GA_ReadOnly)
    except RuntimeError:
        logger.exception("Could not open GRIB file")
        raise SystemExit
    source_projection = grib_ds.GetProjection()
    source_geotransform = grib_ds.GetGeoTransform()
    bands_to_process = extract_bands(grib_ds, [filter_rules],
                                     step_ge=step_ge, step_le=step_le)
    u_bands = [band for band in bands_to_process
               if band.GetMetadataItem(u_filter[0]) == u_filter[1]]
    v_bands = [band for band in bands_to_process
               if band.GetMetadataItem(v_filter[0]) == v_filter[1]]
    for index, (u_band,v_band) in enumerate(
        [(u,v) for u,v in product(u_bands, v_bands)
        if (u.GetMetadataItem(couple_keyword) ==
            v.GetMetadataItem(couple_keyword))]):
        print(index)
        logger.debug("Processing band {}/{}...".format(index+1, len(u_bands)))
        output_path = os.path.join(output_dir, render_str_template(
            u_band.GetMetadata(),
            output_filename_template
        ))
        components = []
        for grib_band in (u_band, v_band):
            mem_ds = process_band(grib_band, source_projection,
                                  source_geotransform, scale_factor,
                                  epsg_code, resample_factor)
            components.append(mem_ds)
        if compute_module:
            write_wind_module(components, output_path, driver_name)
        else:
            write_wind_components(components, output_path, driver_name)


def extract_wind_speed_to_multilayer_netcdf(
    grib_path, output_dir, output_filename_template=None,
    u_filter=('GRIB_ELEMENT','10U'), v_filter=('GRIB_ELEMENT','10V'),
    common_filter={'GRIB_SHORT_NAME':'0-SFC'},
    couple_keyword='GRIB_FORECAST_SECONDS',
    epsg_code=None, scale_factor=1., resample_factor=1.,
    extraction_method='multilayer', step_ge=0, step_le=72):
    compute_module = ("compute_module" in extraction_method)
    filter_rules = common_filter.copy()
    filter_rules.update({u_filter[0]:[u_filter[1]]})
    filter_rules[v_filter[0]].append(v_filter[1])
    output_filename_template = (
        output_filename_template or (
            DEFAULT_MODULE_FILENAME_TEMPLATE
            if compute_module else DEFAULT_COMPONENTS_FILENAME_TEMPLATE
        )
    )
    try:
        grib_ds = gdal.Open(grib_path, gdal.GA_ReadOnly)
    except RuntimeError:
        logger.exception("Could not open GRIB file")
        raise SystemExit
    source_projection = grib_ds.GetProjection()
    source_geotransform = grib_ds.GetGeoTransform()
    bands_dict = extract_bands_dict(grib_ds, [filter_rules],
                                    step_ge=step_ge, step_le=step_le)
    u_bands = bands_dict[u_filter[1]]
    v_bands = bands_dict[v_filter[1]]
    if any(bands_dict):
        base_grib_band = list(bands_dict.values())[0][0][1]
        ref_time = dt.datetime(1970, 1, 1) + dt.timedelta(
            seconds=int(base_grib_band.GetMetadataItem(
                'GRIB_REF_TIME').split()[0])
        )
        mem_ds = process_band(base_grib_band, source_projection,
                              source_geotransform, scale_factor,
                              epsg_code, resample_factor)
        output_path = _get_output_path(mem_ds, output_dir,
                                       output_filename_template,
                                       [filter_rules])
        _create_netcdf_base(mem_ds, output_path)
        size_t = max([len(bands_list) for bands_list in bands_dict.values()])
        dims_settings_dict = OrderedDict([
            ('wind_component', {'size': 2, 'attrs': {
                'long_name': np.array("wind_component",'|S'),
                'axis': np.array("C",'|S'),
                'components': np.array("u, v",'|S'),
                'values': [0, 1],
            }}),
            ('time', {'size': size_t, 'attrs': {
                'NAME': np.array("time",'|S'),
                'standard_name': np.array("time",'|S'),
                'long_name': np.array("time",'|S'),
                'axis': np.array("T",'|S'),
                'units': np.array(
                    ref_time.strftime("hours since %Y-%m-%d %H:%M:00"),'|S'
                ),
            }}),
        ])
        if compute_module:
            dims_settings_dict.pop('wind_component');
        nc_fid, dims = _reset_netcdf_multidim(output_path, dims_settings_dict)
        grid_mapping_name, = [k for k,v in nc_fid.items()
                              if 'grid_mapping_name' in v.attrs.keys()]
        dummy_data = base_grib_band.ReadAsArray()
        datatype = dummy_data.dtype
        dummy_data = None
        nc_var = nc_fid.create_dataset(
            "UV", [dim.size for dim in dims], datatype,
            compression="gzip", compression_opts=9
        )
        nc_var.attrs.create('grid_mapping', np.array(grid_mapping_name,'|S'))
        time_dim = None
        for dim_idx, dim in enumerate(dims):
            nc_var.dims[dim_idx].attach_scale(dim)
            if dim.name == '/time':
                time_dim = dim
        for layer_idx, (u_band,v_band) in enumerate(
            [(u,v) for u,v in product(u_bands, v_bands)
            if (u[1].GetMetadataItem(couple_keyword) ==
                v[1].GetMetadataItem(couple_keyword))]):
            step = u_band[0]
            if time_dim is not None:
                time_dim[layer_idx] = step
            logger.debug("Processing band {}/{}...".format(layer_idx+1,
                                                           len(u_bands)))
            components = []
            for c_idx, grib_band in enumerate([u_band[1], v_band[1]]):
                mem_ds = process_band(grib_band, source_projection,
                                      source_geotransform, scale_factor,
                                      epsg_code, resample_factor)
                components.append(mem_ds.ReadAsArray())
            if compute_module:
                nc_var[layer_idx, :, :] = np.sqrt(
                    components[0]**2 + components[1]**2
                )
            else:
                (nc_var[0, layer_idx, :, :],
                 nc_var[1, layer_idx, :, :]) = components
            mem_band = mem_ds.GetRasterBand(1)
            map_atts = {
                'units': np.array(mem_band.GetMetadataItem('GRIB_UNIT'),'|S'),
                'long_name': np.array("uv wind components [m/s]",'|S'),
                '_FillValue': mem_band.GetNoDataValue(),
                'scale_factor': (mem_band.GetScale()
                                 if mem_band.GetScale() != 1.
                                 else None),
                'add_offset': (mem_band.GetOffset()
                                 if mem_band.GetOffset() != 0
                                 else None),
            }
            for key, val in map_atts.items():
                if (val is not None and
                    nc_var.attrs.get(key) is None):
                    nc_var.attrs.create(key, val)
        nc_fid.flush()
        nc_fid.close()
    else:
        logger.debug("No bands were found with the given filter...")


def write_wind_module(wind_components, output_path,
                      gdal_driver_name='GTiff'):
    u_comp_ds, v_comp_ds = wind_components
    module_ds = gdal.GetDriverByName('MEM').CreateCopy('', u_comp_ds)
    module_band = module_ds.GetRasterBand(1)
    module_band.WriteArray(np.sqrt(
        u_comp_ds.ReadAsArray()**2 + v_comp_ds.ReadAsArray()**2
    ))
    metadata = module_band.GetMetadata()
    metadata.pop('GRIB_COMMENT', None)
    metadata['GRIB_ELEMENT'] = 'wind speed module'
    module_band.SetMetadata(metadata)
    mem_dataset_to_file(output_path, module_ds,
                        driver_name=gdal_driver_name)


def write_wind_components(wind_components, output_path,
                          driver_name='Json'):
    if driver_name == 'Json':
        json_content = [mem_dataset_to_dict(mem_ds)
                        for mem_ds in wind_components]
        with open(output_path, "w") as jsonfile:
            json.dump(json_content, jsonfile)
    elif driver_name == 'WebGL':
        wind_components_to_webgl_png(wind_components, output_path)
    else:
        u_comp_ds, v_comp_ds = wind_components
        trg_ds = gdal.GetDriverByName('MEM').CreateCopy('', u_comp_ds)
        v_band = v_comp_ds.GetRasterBand(1)
        trg_ds.AddBand(v_band.DataType)
        trg_ds.GetRasterBand(2).WriteArray(v_band.ReadAsArray())
        mem_dataset_to_file(output_path, trg_ds,
                            driver_name=driver_name)


def wind_components_to_webgl_png(components, output_path):
    u_band, v_band = [mem_ds.GetRasterBand(1) for mem_ds in components]
    u_data, v_data = [band.ReadAsArray() for band in (u_band, v_band)]
    u_min, u_max, v_min, v_max = (u_data.min(), u_data.max(),
                                  v_data.min(), v_data.max())
    u_norm, v_norm = ((u_data[:,:] - u_min) / (u_max - u_min) * 255,
                      (v_data[:,:] - v_min) / (v_max - v_min) * 255)
    u_icomp = np.array(np.floor(u_norm), 'u1')
    u_dcomp = np.array((u_norm - u_icomp) * 255, 'u1')
    v_icomp = np.array(np.floor(v_norm), 'u1')
    v_dcomp = np.array((v_norm - v_icomp) * 255, 'u1')
    wind_data = np.array((u_icomp.T, u_dcomp.T,
                          v_icomp.T, v_dcomp.T)).T
    img = Image.fromarray(wind_data, "RGBA")
    img.save(output_path, "PNG")

    geot = components[0].GetGeoTransform()
    json_content = {
        "uMin": u_min, "uMax": u_max, "vMin": v_min, "vMax": v_max,
        "nx": u_band.XSize, "ny": u_band.YSize, "numberPoints": u_data.size,
        "la1": geot[3] + geot[5] / 2., "lo1": geot[0] + geot[1] / 2.,
        "la2": geot[3] + geot[5] / 2. + geot[5] * (u_band.YSize - 1),
        "lo2": geot[0] + geot[1] / 2. + geot[1] * (u_band.XSize - 1),
        "dx": geot[1], "dy": -1 * geot[5],
    }
    with open(output_path.replace('.png','.json'), "w") as jsonfile:
        json.dump(json_content, jsonfile)


def mem_dataset_to_dict(mem_ds):
    src_band = mem_ds.GetRasterBand(1)
    data = src_band.ReadAsArray()
    src_geot = mem_ds.GetGeoTransform()
    json_content = {
        "header" : {"nx": src_band.XSize,
                    "ny": src_band.YSize,
                    "numberPoints": data.size,
                    "la1": src_geot[3] + src_geot[5] / 2.,
                    "la2": src_geot[3] + src_geot[5] / 2. +\
                           src_geot[5] * (src_band.YSize - 1),
                    "lo1": src_geot[0] + src_geot[1] / 2.,
                    "lo2": src_geot[0] + src_geot[1] / 2. +\
                           src_geot[1] * (src_band.XSize - 1),
                    "dx": src_geot[1],
                    "dy": -1 * src_geot[5]},
        "data": [round(d, 2) for d in data.reshape(data.size)]
    }
    return json_content


#if __name__ == "__main__":
#    process_wind_speed_components(*sys.argv[1:])
