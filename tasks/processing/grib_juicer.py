#!/usr/bin/env python
# -*- coding: utf8 -*-

# MIT License
# 
# Copyright (c) 2017 likeno
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


import argparse
import datetime as dt
from itertools import product
from collections import OrderedDict
import os
import logging

import json
from jinja2 import Environment
from osgeo import gdal
from osgeo import ogr
from osgeo import osr

import numpy as np
import h5py

logger = logging.getLogger(__name__)
gdal.UseExceptions()

DEFAULT_FILTER_RULES = [
    {'GRIB_ELEMENT': ['RH', 'TMP'],  # relative humidity and temperature
     'GRIB_SHORT_NAME': ['2-HTGL']},
    {'GRIB_ELEMENT': ['UGRD', 'VGRD'],  # wind (u,v)
     'GRIB_SHORT_NAME': ['10-HTGL']},
    {'GRIB_ELEMENT': ['PRMSL'],   # pressure
     'GRIB_SHORT_NAME': ['0-MSL']},
]

DEFAULT_OUTPUT_FILENAME_TEMPLATE = (
    "AROME_SMALL_PT2_025_{{ GRIB_ELEMENT }}_{{ GRIB_SHORT_NAME }}_"
    "{{ REF_TIME | format('%Y%m%d') }}_{{ FORECAST_TIME_STEP }}"
)


def contour_generate(layer_name, src_projection, trg_subdataset,
                     src_band, contour_interval, contour_base):
    trg_srs = osr.SpatialReference()
    trg_srs.ImportFromWkt(src_projection)
    trg_layer = trg_subdataset.CreateLayer(layer_name, srs=trg_srs,
                                           geom_type=ogr.wkbLineString)
    trg_layer.CreateField(ogr.FieldDefn("value", ogr.OFTInteger))
    trg_feature_def = trg_layer.GetLayerDefn()
    tmp_layer = trg_subdataset.CreateLayer('tmp', srs=trg_srs)
    gdal.ContourGenerate(src_band, contour_interval,
                         contour_base, [], 0, 0, tmp_layer, -1, -1)
    for feature_id in range(tmp_layer.GetFeatureCount()):
        trg_feature = ogr.Feature(trg_feature_def)
        trg_line = ogr.Geometry(ogr.wkbLineString)
        tmp_feature = tmp_layer.GetFeature(feature_id)
        tmp_line = tmp_feature.geometry()
        for point_id in range(tmp_line.GetPointCount()):
            x, y, value = tmp_line.GetPoint(point_id)
            trg_line.AddPoint(x, y)
        trg_feature.SetGeometry(trg_line)
        trg_feature.SetField("value", value)
        trg_feature.SetFID(feature_id)
        trg_layer.CreateFeature(trg_feature)
        trg_line.Destroy()
        trg_feature.Destroy()
    trg_subdataset.DeleteLayer('tmp')


def mem_dataset_to_contour_shapefile(output_fname, mem_ds,
                                     contour_interval=2,
                                     contour_base=0):
    src_band = mem_ds.GetRasterBand(1)
    src_projection = mem_ds.GetProjection()
    shapefile_driver = ogr.GetDriverByName('ESRI Shapefile')
    layer_name = "_".join([
        src_band.GetMetadataItem('GRIB_ELEMENT'),
        src_band.GetMetadataItem('GRIB_SHORT_NAME')
    ])
    # layer_name = os.path.basename(output_fname)
    output_dir = os.path.dirname(output_fname)
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    trg_subdataset = shapefile_driver.CreateDataSource(output_fname)
    contour_generate(layer_name, src_projection, trg_subdataset,
                     src_band, contour_interval, contour_base)
    trg_subdataset.Destroy()
    shapefile_driver = None


def mem_dataset_to_file(output_fname, mem_ds,
                        driver_name='GTiff', options=[]):
    if driver_name == 'netCDF':
        options = ['FORMAT=NC4','COMPRESS=DEFLATE','ZLEVEL=9']
    output_dir = os.path.dirname(output_fname)
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    trg_subdataset = gdal.GetDriverByName(
        driver_name
    ).CreateCopy(output_fname, mem_ds, options=options)
    trg_subdataset.FlushCache()


def to_one_band_mem_driver(src_band, src_projection, src_geotransform,
                           scale_factor=1.):
    mem_ds = gdal.GetDriverByName('MEM').Create(
        '', src_band.XSize, src_band.YSize,
        1, src_band.DataType
    )
    mem_ds.SetProjection(src_projection)
    mem_ds.SetGeoTransform(src_geotransform)
    mem_band = mem_ds.GetRasterBand(1)
    mem_band.SetScale(src_band.GetScale())
    mem_band.SetOffset(src_band.GetOffset())
    if src_band.GetNoDataValue() is not None:
        mem_band.SetNoDataValue(src_band.GetNoDataValue())
    mem_band.SetMetadata(src_band.GetMetadata())
    data = src_band.ReadAsArray()
    if scale_factor != 1.:
        data *= scale_factor
        mem_band.SetScale(mem_band.GetScale()/scale_factor)
    mem_band.WriteArray(data)
    return mem_ds


def reproject_dataset(src_subdataset, epsg_code, error_threshold=0.125,
                      method=gdal.GRA_NearestNeighbour):
    srs = osr.SpatialReference()
    srs.ImportFromEPSG(epsg_code)
    trg_subdataset = src_subdataset
    if src_subdataset.GetProjection() != srs.ExportToWkt():
        logger.debug("\tReprojecting data...")
        trg_subdataset = gdal.AutoCreateWarpedVRT(
            src_subdataset, trg_subdataset.GetProjection(),
            srs.ExportToWkt(), method, error_threshold
        )
        src_band = src_subdataset.GetRasterBand(1)
        trg_band = trg_subdataset.GetRasterBand(1)
        trg_band.SetScale(src_band.GetScale())
        trg_band.SetOffset(src_band.GetOffset())
        if src_band.GetNoDataValue() is not None:
            trg_band.SetNoDataValue(src_band.GetNoDataValue())
    return trg_subdataset


def resample_dataset(src_dataset, resample_factor,
                     error_threshold=0.125,
                     method=gdal.GRA_Bilinear):
                           #gdal.GRA_NearestNeighbour
    src_proj = src_dataset.GetProjection()
    src_geot = src_dataset.GetGeoTransform()
    src_band = src_dataset.GetRasterBand(1)
    trg_shape = (
        int(round(src_band.XSize * resample_factor)),
        int(round(src_band.YSize * resample_factor))
    )
    trg_subdataset = gdal.GetDriverByName('MEM').Create(
        '', trg_shape[0], trg_shape[1], 1, src_band.DataType
    )
    trg_res = (src_geot[1] / resample_factor,
               src_geot[5] / resample_factor)
    trg_geot = (src_geot[0] + src_geot[1] / 2. - trg_res[0] / 2.,
                trg_res[0], 0.0,
                src_geot[3] + src_geot[5] / 2. - trg_res[1] / 2.,
                0.0, trg_res[1])
    trg_subdataset.SetGeoTransform(trg_geot)
    trg_subdataset.SetProjection(src_proj)
    gdal.ReprojectImage(src_dataset, trg_subdataset,
                        src_proj, src_proj, method,
                        maxerror=error_threshold)
    trg_band = trg_subdataset.GetRasterBand(1)
    trg_band.SetScale(src_band.GetScale())
    trg_band.SetOffset(src_band.GetOffset())
    if src_band.GetNoDataValue() is not None:
        trg_band.SetNoDataValue(src_band.GetNoDataValue())
    return trg_subdataset


def process_band(source_band, source_projection, source_geotransform,
                 scale_factor, epsg_code, resample_factor):
    mem_ds = to_one_band_mem_driver(
        source_band, source_projection, source_geotransform, scale_factor
    )
    if epsg_code is not None:
        mem_ds = reproject_dataset(mem_ds, epsg_code)
    if resample_factor != 1.:
        mem_ds = resample_dataset(mem_ds, resample_factor)
    return mem_ds

def extract_bands(grib_dataset, filter_rules,
                  time_keyword='GRIB_FORECAST_SECONDS',
                  step_ge=0, step_le=72):
    band_indexes = range(1, grib_dataset.RasterCount+1)
    extracted_bands = set()
    for band_index, rule in product(band_indexes, filter_rules):
        raster_band = grib_dataset.GetRasterBand(band_index)
        step = int(raster_band.GetMetadataItem(time_keyword).split()[0]) / 3600
        if (match_filter_rule(raster_band, **rule) and
            step >= step_ge and step <= step_le):
            extracted_bands.add(raster_band)
    return list(extracted_bands)


def extract_bands_dict(grib_dataset, filter_rules,
                       element_keyword="GRIB_ELEMENT",
                       time_keyword='GRIB_FORECAST_SECONDS',
                       step_ge=0, step_le=72):
    bands_list = extract_bands(grib_dataset, filter_rules,
                               step_ge=step_ge, step_le=step_le)
    elements_dict = {}
    while any(bands_list):
        grib_element = bands_list[-1].GetMetadataItem(element_keyword)
        elements_dict[grib_element] = sorted([
            (int(bands_list[idx].GetMetadataItem(
                 time_keyword
             ).split()[0]) / 3600, bands_list.pop(idx))
            for idx in range(len(bands_list)-1,-1,-1)
            if bands_list[idx].GetMetadataItem(element_keyword) == grib_element
        ])
    return elements_dict


def match_filter_rule(raster_band, **metadata_combinations):
    result = True
    for metadata_key, filter_values in metadata_combinations.items():
        existing_value = raster_band.GetMetadataItem(metadata_key)
        if existing_value is None or existing_value not in filter_values:
            result = False
    return result


def render_str_template(grib_metadata, str_template):
    context = grib_metadata.copy()
    grib_ref_time = context.get('GRIB_REF_TIME')
    if grib_ref_time is not None:
        context['REF_TIME'] = dt.datetime(1970, 1, 1) + dt.timedelta(
            seconds=int(grib_ref_time.split()[0])
        )
    grib_valid_time = context.get('GRIB_VALID_TIME')
    if grib_valid_time is not None:
        context['VALID_TIME'] = dt.datetime(1970, 1, 1) + dt.timedelta(
            seconds=int(grib_valid_time.split()[0])
        )
    grib_forecast_seconds = context.get('GRIB_FORECAST_SECONDS')
    if grib_forecast_seconds is not None:
        context['FORECAST_TIME_STEP'] = "{:02d}".format(int(int(
            grib_forecast_seconds.split()[0]
        ) / 3600))
    env = Environment()
    env.filters['format'] = lambda d, f: format(d,f)
    output_str = env.from_string(
        str_template
    ).render(**context)
    return output_str


def fix_filter_rules(filter_rules):
    fixed_filter_arg = []
    for raw_filter_list in filter_rules:
        cumulative_filter_rules = {}
        for filter_element in raw_filter_list:
            if filter_element.endswith(':'):
                filter_name = filter_element[:-1]
                filter_values = []
                cumulative_filter_rules[filter_name] = filter_values
            else:
                filter_values.append(filter_element)
        if any(cumulative_filter_rules):
            fixed_filter_arg.append(cumulative_filter_rules)
    return fixed_filter_arg

def _get_output_path(mem_ds, output_dir,
                     output_filename_template, filter_rules):
    base_band = mem_ds.GetRasterBand(1)
    context = base_band.GetMetadata()
    filter_context = {}
    for f in filter_rules:
        for key, val in f.items():
            tmp_str = "_".join(val)
            filter_context[key] = (
                tmp_str if filter_context.get(key) is None
                else "_".join([filter_context[key], tmp_str])
            )
    context.update(filter_context)
    output_path = os.path.join(
        output_dir,
        render_str_template(context, output_filename_template)
    )
    return output_path

def _create_netcdf_base(mem_ds, output_path):
    logger.debug("\tSaving as {}...".format('netCDF'))
    nc_driver = gdal.GetDriverByName('netCDF')
    base_copy = nc_driver.CreateCopy(
        output_path, mem_ds,
        options=['FORMAT=NC4','COMPRESS=DEFLATE','ZLEVEL=9']
    )
    base_copy.FlushCache()
    base_copy, nc_driver = (None, None)
    return output_path


def _reset_netcdf_multidim(filename, dims_orddict):
    gdal_ds = gdal.Open('NETCDF:"{}":Band1'.format(filename))
    metadata = gdal_ds.GetMetadata()
    nc_fid = h5py.File(filename, 'r+')
    (dim_y_name, dim_y),  = nc_fid['Band1'].dims[0].items()
    (dim_x_name, dim_x),  = nc_fid['Band1'].dims[1].items()
    grid_mapping_name, = [k for k,v in nc_fid.items()
                          if 'grid_mapping_name' in v.attrs.keys()]
    # Reset 'bad' format attribute values
    for gdal_attr_name, gdal_attr_value in metadata.items():
        var_name, split_char, attr_name =  gdal_attr_name.partition('#')
        if var_name == 'NC_GLOBAL':
            var_name = '/'
        if var_name in (dim_y_name, dim_x_name, grid_mapping_name, '/'):
            try:
                attr_val = nc_fid[var_name].attrs[attr_name]
            except IOError:
                try:
                    nc_fid[var_name].attrs[attr_name] = gdal_attr_value
                except RuntimeError:
                    pass
    nc_fid['Band1'].dims[0].detach_scale(dim_y)
    nc_fid['Band1'].dims[1].detach_scale(dim_x)
    nc_fid.pop('Band1');
    for key in nc_fid.attrs.keys():
        if key in ('history', 'GDAL'):
            nc_fid.attrs.__delitem__(key)
    dims_list = []
    for dim_idx, (dim_name,
                  dim_props) in enumerate(dims_orddict.items()):
        dim_size = dim_props['size']
        dim_attrs = dim_props['attrs']
        dim = nc_fid.create_dataset(dim_name, (dim_size,), np.int32,
                                    range(dim_size))
        dim.attrs.create('_Netcdf4Dimid', np.array(dim_idx, np.int32))
        for attr_name, attr_value in dim_attrs.items():
            dim.attrs.create(attr_name, attr_value)
        dims_list.append(dim)
    dim_y[:] = np.flipud(dim_y[:]) # Flip up-down y axis...
    dim_y.attrs.create('axis', np.array("Y",'|S'))
    dim_y.attrs['_Netcdf4Dimid'] = len(dims_list)
    dim_x.attrs.create('axis', np.array("X",'|S'))
    dim_x.attrs['_Netcdf4Dimid'] = len(dims_list) + 1
    dims_list.extend([dim_y, dim_x])
    return nc_fid, dims_list


def extract_to_multilayer_netcdf(grib_path, output_dir,
                                 output_filename_template=None,
                                 filter_rules=None, epsg_code=3857,
                                 scale_factor=1., resample_factor=1.,
                                 step_ge=0, step_le=72):
    element_keyword='GRIB_ELEMENT'
    time_keyword='GRIB_FORECAST_SECONDS'
    filter_rules = filter_rules or DEFAULT_FILTER_RULES
    output_filename_template = (
        output_filename_template or DEFAULT_OUTPUT_FILENAME_TEMPLATE
    )
    try:
        grib_dataset = gdal.Open(grib_path, gdal.GA_ReadOnly)
    except RuntimeError:
        logger.exception("Could not open GRIB file")
        raise SystemExit
    source_projection = grib_dataset.GetProjection()
    source_geotransform = grib_dataset.GetGeoTransform()
    bands_dict = extract_bands_dict(grib_dataset, filter_rules,
                                    step_ge=step_ge, step_le=step_le)
    if any(bands_dict):
        base_grib_band = list(bands_dict.values())[0][0][1]
        ref_time = dt.datetime(1970, 1, 1) + dt.timedelta(
            seconds=int(base_grib_band.GetMetadataItem('GRIB_REF_TIME').split()[0])
        )
        mem_ds = process_band(base_grib_band, source_projection,
                              source_geotransform, scale_factor,
                              epsg_code, resample_factor)
        output_path = _get_output_path(mem_ds, output_dir, 
                                       output_filename_template,
                                       filter_rules)
        _create_netcdf_base(mem_ds, output_path)
        # Assuming all elements are represented
        # for the same forcast time steps...
        size_t = len(list(bands_dict.values())[0])
        nc_fid, (dim0, dim1, dim2) = _reset_netcdf_multidim(output_path,
                                                            OrderedDict([
            ('time', {'size': size_t, 'attrs': {
                'NAME': np.array("time",'|S'),
                'standard_name': np.array("time",'|S'),
                'long_name': np.array("time",'|S'),
                'axis': np.array("T",'|S'),
                'units': np.array(
                    ref_time.strftime("hours since %Y-%m-%d %H:%M:00"),'|S'
                )
            }})
        ]))
        grid_mapping_name, = [k for k,v in nc_fid.items()
                              if 'grid_mapping_name' in v.attrs.keys()]
        for grib_element, bands_to_process in bands_dict.items():
            first_band = bands_to_process[0][1]
            dummy_data = first_band.ReadAsArray()
            datatype = dummy_data.dtype
            dummy_data = None
            nc_var = nc_fid.create_dataset(
                grib_element, (size_t, dim1.size, dim2.size), datatype,
                compression="gzip", compression_opts=9, # shuffle=True,
                fillvalue=first_band.GetNoDataValue()
            )
            nc_var.attrs.create('grid_mapping', np.array(grid_mapping_name,'|S'))
            for dim_idx, dim in ((0, dim0), (1, dim1), (2, dim2)):
                nc_var.dims[dim_idx].attach_scale(dim)
            for layer_idx, (step, grib_band) in enumerate(bands_to_process):
                logger.debug(
                    "Processing band {}/{}...".format(
                        layer_idx+1, len(bands_to_process)
                    )
                )
                mem_ds = process_band(grib_band, source_projection,
                                      source_geotransform, scale_factor,
                                      epsg_code, resample_factor)
                nc_var[layer_idx,:,:] = mem_ds.ReadAsArray()
                dim0[layer_idx] = step
                mem_band = mem_ds.GetRasterBand(1)
                map_atts = {
                    'units': np.array(mem_band.GetMetadataItem('GRIB_UNIT'),'|S'),
                    'long_name': np.array(mem_band.GetMetadataItem('GRIB_COMMENT'),'|S'),
                    '_FillValue': mem_band.GetNoDataValue(),
                    'scale_factor': (mem_band.GetScale()
                                     if mem_band.GetScale() != 1.
                                     else None),
                    'add_offset': (mem_band.GetOffset()
                                     if mem_band.GetOffset() != 0
                                     else None),
                }
                for key, val in map_atts.items():
                    if (val is not None and
                        nc_var.attrs.get(key) is None):
                        nc_var.attrs.create(key, val)

        nc_fid.flush()
        nc_fid.close()
    else:
        logger.debug("No bands were found with the given filter...")


def extract_to_singlelayer_files(grib_path, output_dir,
                              output_filename_template=None,
                              filter_rules=None, epsg_code=3857,
                              driver_name="GTiff",
                              scale_factor=1., resample_factor=1.,
                              step_ge=0, step_le=72):
    filter_rules = filter_rules or DEFAULT_FILTER_RULES
    output_filename_template = (
        output_filename_template or DEFAULT_OUTPUT_FILENAME_TEMPLATE
    )
    try:
        grib_dataset = gdal.Open(grib_path, gdal.GA_ReadOnly)
    except RuntimeError:
        logger.exception("Could not open GRIB file")
        raise SystemExit
    source_projection = grib_dataset.GetProjection()
    source_geotransform = grib_dataset.GetGeoTransform()
    bands_to_process = extract_bands(grib_dataset, filter_rules,
                                     step_ge=step_ge, step_le=step_le)
    for index, grib_band in enumerate(bands_to_process):
        logger.debug(
            "Processing band {}/{}...".format(index+1, len(bands_to_process))
        )
        output_path = os.path.join(
            output_dir,
            render_str_template(grib_band.GetMetadata(),
                                output_filename_template)
        )
        mem_ds = process_band(grib_band, source_projection,
                              source_geotransform, scale_factor,
                              epsg_code, resample_factor)
        logger.debug("\tSaving as {}...".format(driver_name))
        if driver_name == 'ESRI Shapefile':
            mem_dataset_to_contour_shapefile(output_path, mem_ds)
        else:
            mem_dataset_to_file(output_path, mem_ds,
                                driver_name=driver_name)


def extract_data(*args, **kwargs):
    extraction_method = kwargs.pop('extraction_method')
    if (extraction_method is not None and
        'multilayer' in extraction_method):
        if kwargs.get('driver_name') is not None:
            kwargs.pop('driver_name')
        extract_to_multilayer_netcdf(*args, **kwargs)
    else:
        extract_to_singlelayer_files(*args, **kwargs)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter,
        description=(
            "Usage example:\n"
            "./grib_juicer.py /data/raw_data/ecmwf/2017/11/10/"
            "ECMWF_OPER_001_FC_SP_ATP_090_2017111012 "
            "/data/output/ecmwf/2017/11/10/run_12 -ot "
            "ECMWF_OPER_001_FC_SP_ATP_090_T_850-ISBL_2017111012.nc "
            "-f GRIB_ELEMENT: T GRIB_SHORT_NAME: 850-ISBL -p 3857 -m -v"
        )
    )
    parser.add_argument("grib_path", help="Input grib file path")
    parser.add_argument("output_dir", help="Output directory")
    parser.add_argument(
        "-ot",
        "--output_filename_template",
        help="Template string to define the output filenames.\nMetadata "
             "keywords can be used to be replaced \nwith the respective values "
             "of each band matching \nthe given filter_rules. \nDefaults to: \n"
             "%(default)r\n",
        default=DEFAULT_OUTPUT_FILENAME_TEMPLATE
    )
    parser.add_argument(
        '-f',
        '--filter_rules',
        nargs='*',
        action='append',
        default=[],
        help="Filter rules are based on metadata keywords and values.\nEach "
             "filter rule is a list of band metadata\nkeywords followed by "
             "the respective values to be\n retrieved.\nUse the token ': ' to "
             "separate each keyword from\nthe respective values and ' ' to "
             "separate each value.\nOne can add as many filter rules as "
             "needed.\nNote that each filter rule is cumulative so \nadding "
             "different filter works as an OR operator.\nFor instance:\n\n"
             "\t-f GRIB_ELEMENT: TMP RH GRIB_SHORT_NAME: 2-HTGL -f "
             "GRIB_ELEMENT: UGRD VGRD GRIB_SHORT_NAME: 10-HTGL\n\nWorks as "
             "the following logic expression:\n\n\t[((GRIB_ELEMENT == TMP) or "
             "(GRIB_ELEMENT == RH)) and GRIB_SHORT_NAME == 2-HTGL] or "
             "[((GRIB_ELEMENT == UGRD) or (GRIB_ELEMENT == VGRD)) and "
             "GRIB_SHORT_NAME == 10-HTGL]\n\n\nDefaults to:\n"
             "-f GRIB_ELEMENT: RH TMP GRIB_SHORT_NAME: 2-HTGL "
             "-f GRIB_ELEMENT: UGRD VGRD GRIB_SHORT_NAME: 10-HTGL "
             "-f GRIB_ELEMENT: PRMSL GRIB_SHORT_NAME: 0-MSL "
    )
    parser.add_argument(
        "-d", "--driver_name", default="GTiff",
        help="Gdal driver name of the output file format."
    )
    parser.add_argument(
        "-r", "--resample_factor", type=float, default=1.,
        help="Resample factor to be applied. The resample factor\n"
             "is used to decrese the data spatial resolution.\n"
             "The spatial resolution value should be between 0 and 1."
    )
    parser.add_argument(
        "-s", "--scale_factor", type=float, default=1.,
        help="Scale factor to be applied."
    )
    parser.add_argument(
        "-p", "--projection_epsg", type=int, default=None,
        help="Output projection EPSG code. (Use 3857 for web mercator)"
    )
    parser.add_argument(
        "-m","--merge_to_netcdf", action="store_true",
        help="Extract variables into a netCDF4 file format\n"
             "with multiple layers. Each layer representing\n"
             "a time step given by the GRIB_FORECAST_SECONDS\n"
             "attribute value."
    )
    parser.add_argument(
        "-g", "--step_ge", type=int, default=0,
        help="Truncate the retrieved layers with a\n"
             "minimum value in hours. (Basede on \n"
             "GRIB_FORECAST_SECONDS attribute value."
    )
    parser.add_argument(
        "-l", "--step_le", type=int, default=72,
        help="Truncate the retrieved layers with a\n"
             "maximum value in hours. (Based on \n"
             "GRIB_FORECAST_SECONDS attribute value."
    )
    parser.add_argument(
        "-v","--verbose", action="store_true",
        help="Be more verbose during execution."
    )
    args = parser.parse_args()
    filter_rules = args.filter_rules if any(
        args.filter_rules) else DEFAULT_FILTER_RULES
    logging.basicConfig(
        level=logging.DEBUG if args.verbose else logging.WARNING)
    if any(args.filter_rules):
        fixed_rules = fix_filter_rules(args.filter_rules)
    else:
        fixed_rules = DEFAULT_FILTER_RULES
    logger.debug("filter rules: {}".format(fixed_rules))
    kwargs = {
        "output_filename_template": args.output_filename_template,
        "filter_rules": fixed_rules,
        "epsg_code": args.projection_epsg,
        "scale_factor": args.scale_factor,
        "resample_factor": args.resample_factor,
        "step_ge": args.step_ge,
        "step_le": args.step_le
    }
    if args.merge_to_netcdf:
        extract_to_multilayer_netcdf(args.grib_path, args.output_dir,
                                     **kwargs)
    else:
        extract_to_one_band_files(args.grib_path, args.output_dir,
                                  driver_name=args.driver_name,
                                  **kwargs)
