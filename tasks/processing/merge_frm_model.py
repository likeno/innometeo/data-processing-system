#!/usr/bin/env python
# -*- coding: utf8 -*-

# MIT License
#
# Copyright (c) 2017 likeno
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


from osgeo import gdal, osr
import h5py
import numpy as np
import os
from xml.etree import ElementTree as ET
import uuid

def fix_frm(input_paths, output_path, epsg_code=3857,
            var_names=['FWI', 'ISI', 'BUI', 'FFMC', 'DC', 'DMC', 'RCM', 'PERC_FWI']):

    if any(input_paths):

        for var_num, input_path in enumerate(input_paths):
            inp_subdataset = gdal.Open(input_path, gdal.GA_ReadOnly)
            data_type = inp_subdataset.GetRasterBand(1).DataType
            gdal_metadata = inp_subdataset.GetMetadata()
            h5_attrs = get_h5_attributes_from_gdal_metadata(
                gdal_metadata, ["history", "CDI", "CDO", "esri_pe_string"]
            )
            var_name, = [key for key in h5_attrs.keys() if key in var_names]
            wkt_proj = inp_subdataset.GetMetadataItem(var_name+'#esri_pe_string')
            inp_subdataset.SetProjection(wkt_proj)
            fill_value = inp_subdataset.GetRasterBand(1).GetNoDataValue()

            trg_srs = osr.SpatialReference()
            trg_srs.ImportFromEPSG(epsg_code)

            tmp_subdataset = gdal.AutoCreateWarpedVRT(
                inp_subdataset, None, trg_srs.ExportToWkt(),
                gdal.GRA_NearestNeighbour, 0.125,
            )

            # VRT workaround to init desination data:
            vrt_fname = '/tmp/{}.vrt'.format(uuid.uuid1())
            gdal.GetDriverByName('VRT').CreateCopy(
                vrt_fname, tmp_subdataset
            )
            xml_tree = ET.ElementTree(file=vrt_fname)
            option, = xml_tree.findall('GDALWarpOptions/Option/'
                                       '[@name="INIT_DEST"]')
            option.text = '{}'.format(fill_value)
            xml_tree.write(vrt_fname)
            reproj_subdataset = gdal.Open(vrt_fname)

            geo_transform = reproj_subdataset.GetGeoTransform()
            trg_proj = reproj_subdataset.GetProjection()
            shape = (reproj_subdataset.RasterCount,
                     reproj_subdataset.RasterYSize,
                     reproj_subdataset.RasterXSize)

            if var_num == 0:
                time_values = [int(t) for t in
                               inp_subdataset.GetMetadataItem(
                                   "NETCDF_DIM_time_VALUES"
                               )[1:-1].split(',')]

                dim_x_name, dim_y_name = create_netcdf_base(
                    output_path, shape, data_type,
                    trg_proj, geo_transform, time_values
                )

                nc_fid = h5py.File(output_path, 'r+')
                dim_t = nc_fid["time"]
                dim_y = nc_fid[dim_y_name]
                dim_x = nc_fid[dim_x_name]

            for band_num in range(reproj_subdataset.RasterCount):
                raster_band = reproj_subdataset.GetRasterBand(band_num+1)
                band_data = raster_band.ReadAsArray()
                if band_num == 0:
                    fill_value = h5_attrs[var_name].get('_FillValue')
                    create_args = {
                        "shape": shape,
                        "dtype": band_data.dtype,
                        "compression": "gzip",
                        "compression_opts": 9
                    }
                    if fill_value is not None:
                        create_args.update({"fillvalue": np.array(fill_value,
                                                                  band_data.dtype)})
                    print("Create variable: {}".format(var_name))
                    nc_var = nc_fid.create_dataset(var_name, **create_args)
                nc_var[band_num, :, :] = band_data
            reproj_subdataset = None
            os.remove(vrt_fname)

            # Restore metadata:
            for h5obj_name, attrs in h5_attrs.items():
                h5obj = nc_fid.get(h5obj_name)
                if h5obj is not None:
                    for attr_name, value in attrs.items():
                        if (h5obj.attrs.get(attr_name) is None and
                            attr_name not in ("missing_value")):
                            if attr_name == "_FillValue":
                                value = np.array(value, band_data.dtype)
                            try:
                                tmp_value = h5obj.attrs.get(attr_name)
                                if tmp_value is None:
                                    try:
                                        h5obj.attrs.create(attr_name, value)
                                    except RuntimeError:
                                        pass
                            except IOError:
                                try:
                                    h5obj.attrs.__delitem__(attr_name)
                                    h5obj.attrs.create(attr_name, value)
                                except RuntimeError:
                                    pass

            grid_mapping_name, = [k for k,v in nc_fid.items()
                                  if 'grid_mapping_name' in v.attrs.keys()]
            nc_var.attrs.create('grid_mapping', np.array(grid_mapping_name, '|S'))

            # Reattach dimensions scale:
            for dim_idx, dim in enumerate([dim_t, dim_y, dim_x]):
                nc_var.dims[dim_idx].attach_scale(dim)
        nc_fid.close()

    else:
        print("Missing input paths!")


def create_netcdf_base(output_path, shape, data_type, proj,
                       geo_transform, time):
    if os.path.isfile(output_path):
        os.remove(output_path)
    output_dir = os.path.dirname(output_path)
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    out_subdataset = gdal.GetDriverByName('netCDF').Create(
        output_path, shape[2], shape[1], 1, data_type,
        options=['FORMAT=NC4', 'COMPRESS=DEFLATE', 'ZLEVEL=9']
    )
    out_subdataset.SetProjection(proj)
    out_subdataset.SetGeoTransform(geo_transform)
    out_subdataset = None

    # Get attributes:
    out_subdataset = gdal.Open(output_path)
    gdal_metadata = out_subdataset.GetMetadata()
    h5_attrs = get_h5_attributes_from_gdal_metadata(gdal_metadata)
    out_subdataset = None

    nc_fid = h5py.File(output_path, 'r+')

    # Detach dimensions scale:
    (dim_y_name, dim_y), = nc_fid['Band1'].dims[0].items()
    (dim_x_name, dim_x), = nc_fid['Band1'].dims[1].items()
    nc_fid['Band1'].dims[0].detach_scale(dim_y)
    nc_fid['Band1'].dims[1].detach_scale(dim_x)
    for dim_name in (dim_x_name, dim_y_name):
        dim_refs = nc_fid[dim_name].attrs.get('REFERENCE_LIST')
        if dim_refs is not None and any(dim_refs):
            nc_fid[dim_name].attrs['REFERENCE_LIST'] = np.array(
                [], dim_refs.dtype
            )
    nc_fid.pop('Band1')

    # Cleanup metadata:
    for var_name, nc_var in list(nc_fid.items()) + [(u"/", nc_fid["/"])]:
        for attr_name in nc_var.attrs.keys():
            if attr_name in ('GDAL', 'history',
                             '_NCProperties', '_Netcdf4Dimid'):
                nc_var.attrs.__delitem__(attr_name)
            else:
                try:
                    tmp_val = nc_var.attrs[attr_name]
                except IOError:
                    nc_var.attrs.__delitem__(attr_name)
                    try:
                        attr_value = h5_attrs[var_name][attr_name]
                        nc_var.attrs.create(attr_name, attr_value)
                    except RuntimeError:
                        pass

    dim_t = nc_fid.create_dataset('time', (shape[0],), np.int32, time)
    dim_y[:] = np.flipud(dim_y[:]) # Flip up-down y axis...

    dim_t.attrs.create('axis', np.array("T", '|S'))
    dim_t.attrs.create('_Netcdf4Dimid', np.array(0, np.int32))
    dim_y.attrs.create('axis', np.array("Y", '|S'))
    dim_y.attrs.create('_Netcdf4Dimid', np.array(1, np.int32))
    dim_x.attrs.create('axis', np.array("X", '|S'))
    dim_x.attrs.create('_Netcdf4Dimid', np.array(2, np.int32))
    nc_fid.close()

    return dim_x_name, dim_y_name


def get_h5_attributes_from_gdal_metadata(metadata,
                                         except_attributes=[]):
    h5_attributes = dict()
    for key, value in  metadata.items():
        h5obj_name, mark, attr_name = (
            key.replace("NC_GLOBAL", "/").partition('#')
        )
        if (not h5obj_name.startswith('NETCDF_DIM') and
            attr_name not in except_attributes):
            attrs = h5_attributes.setdefault(h5obj_name, dict())
            try:
                attrs[attr_name] = np.array([int(value)])
            except ValueError:
                try:
                    attrs[attr_name] = np.array([float(value)])
                except ValueError:
                    attrs[attr_name] = np.array(value)
                    if attrs[attr_name].dtype.kind == 'U':
                       attrs[attr_name]  = np.array(value, '|S')
    return h5_attributes
