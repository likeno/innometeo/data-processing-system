#!/usr/bin/env python
# -*- coding: utf8 -*-

# MIT License
# 
# Copyright (c) 2017 likeno
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Convert LSASAF Fire_Radiative_Power-Pixel to shapefile.

Input LSASAF FRP-Pixel is in HDF5 format and gets converted to a point
shapefile (at the moment only the FRP variable is being retrieved
in MW units).

"""

import logging
import os
import sys

import h5py
from osgeo import ogr
from osgeo import osr

logger = logging.getLogger(__name__)


def main(input_path, output_path_prefix, var_name):
    logger.debug("locals: {}".format(locals()))
    h5_fid = h5py.File(input_path, 'r')
    var_ds = h5_fid[var_name]
    lat_ds = h5_fid['LATITUDE']
    lon_ds = h5_fid['LONGITUDE']
    data_table = zip(var_ds.value / var_ds.attrs['SCALING_FACTOR'],
                     lat_ds.value / lat_ds.attrs['SCALING_FACTOR'],
                     lon_ds.value / lon_ds.attrs['SCALING_FACTOR'])

    logger.debug("Create shapefile file: {}".format(output_path_prefix+'.shp'))
    output_dir = os.path.dirname(output_path_prefix)
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    # srs: WGS84
    src_srs = osr.SpatialReference()
    src_srs.ImportFromEPSG(4326)
    shapefile_driver = ogr.GetDriverByName('ESRI Shapefile')
    trg_subdataset = shapefile_driver.CreateDataSource(
        output_path_prefix+'.shp')
    # srs: Web Mercator
    trg_srs = osr.SpatialReference()
    trg_srs.ImportFromEPSG(3857)
    trg_layer = trg_subdataset.CreateLayer(var_name, srs=trg_srs,
                                           geom_type=ogr.wkbPoint)
    trg_layer.CreateField(ogr.FieldDefn("value", ogr.OFTReal))
    feature_def = trg_layer.GetLayerDefn()
    trg_feature = ogr.Feature(feature_def)
    point = ogr.Geometry(ogr.wkbPoint)
    transform = osr.CoordinateTransformation(src_srs, trg_srs)
    for point_id, (var_value, lat, lon) in enumerate(data_table):
        point.AddPoint(lon, lat)
        point.Transform(transform)
        trg_feature.SetGeometry(point)
        trg_feature.SetField("value", var_value)
        trg_feature.SetFID(point_id)
        trg_layer.CreateFeature(trg_feature)
    point.Destroy()
    trg_feature.Destroy()
    trg_subdataset.Destroy()


if __name__ == "__main__":
    main(*sys.argv[1:])
