# MIT License
#
# Copyright (c) 2020 likeno
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Functions for generating grib index files"""

import logging

from soko import packages
import xarray as xr

logger = logging.getLogger(__name__)


def main(retrieved_grib_product: packages.RetrievedMember, **kwargs):
    """Generate a GRIB index file.

    GRIB index files provide a speed boost when working with large files, as
    is the case with the GRIBs used to get data for the innometeo tephigrams

    """
    logger.debug(f"grib_path: {retrieved_grib_product.local_path}")
    with xr.open_dataset(retrieved_grib_product.local_path, engine="cfgrib"):
        # just opening the file is enough in order to make cfgrib generate
        # the index file
        pass
