# MIT License
#
# Copyright (c) 2020 likeno
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Functions for publishing granules in the spatial data server"""

import concurrent.futures
import datetime as dt
import logging
import os
import typing
from collections import namedtuple
from pathlib import Path

import dateutil.parser
import requests
from airflow.configuration import conf
from osgeo import gdal
from soko import packages

logger = logging.getLogger(__name__)

CONTENT_TYPE = "application/vnd.api+json"
PRESSURE_LEVEL_NETCDF_DIMENSION_NAME = "isobaricInhPa"


COVERAGE_NAME_MAP = {
    ("arome", "10u"): "arome-wind-u",
    ("arome", "10v"): "arome-wind-v",
    ("arome", "2d"): "arome-dew-point-temperature",
    ("arome", "t"): "arome-temperature",
    ("arome", "2t"): "arome-temperature",
    ("arome", "hcc"): "arome-high-cloud-cover",
    ("arome", "mcc"): "arome-medium-cloud-cover",
    ("arome", "lcc"): "arome-low-cloud-cover",
    ("arome", "tcc"): "arome-total-cloud-cover",
    ("arome", "msl"): "arome-mean-sea-level-pressure",
    ("arome", "r"): "arome-relative-humidity",
    ("arome", "tp"): "arome-total-precipitation-1h",
    ("arome", "u"): "arome-wind-u",
    ("arome", "v"): "arome-wind-v",
    ("arome", "w"): "arome-wind-vertical",
    ("arome", "z"): "arome-geopotential",
    ("arome", "wind-intensity-10m"): "arome-wind-intensity",
    ("arome", "wind-intensity"): "arome-wind-intensity",
    ("arome", "wind-uv"): "arome-wind-uv",
    ("arome", "wind-uv-10m"): "arome-wind-uv",

    ("ecmwf", "t"): "ecmwf-temperature",
    ("ecmwf", "2t"): "ecmwf-temperature",
    ("ecmwf", "2d"): "ecmwf-dew-point-temperature",
    ("ecmwf", "blh"): "ecmwf-boundary-layer-height",
    ("ecmwf", "cape"): "ecmwf-cape",
    ("ecmwf", "capes"): "ecmwf-capes",
    ("ecmwf", "deg0l"): "ecmwf-deg0l",
    ("ecmwf", "hcct"): "ecmwf-convective-cloud-top-height",
    ("ecmwf", "lcc"): "ecmwf-low-cloud-cover",
    ("ecmwf", "mcc"): "ecmwf-medium-cloud-cover",
    ("ecmwf", "hcc"): "ecmwf-high-cloud-cover",
    ("ecmwf", "msl"): "ecmwf-mean-sea-level-pressure",
    ("ecmwf", "r"): "ecmwf-relative-humidity",
    ("ecmwf", "tcc"): "ecmwf-total-cloud-cover",
    ("ecmwf", "sst"): "ecmwf-sea-surface-temperature",
    ("ecmwf", "ssrd"): "ecmwf-surface-solar-radiation-downwards",
    ("ecmwf", "uvb"): "ecmwf-downward-uv-surface-radiation",
    ("ecmwf", "10fg"): "ecmwf-wind-gust-10meter",
    ("ecmwf", "10fg3"): "ecmwf-wind-gust3-10meter",
    ("ecmwf", "10u"): "ecmwf-wind-u",
    ("ecmwf", "10v"): "ecmwf-wind-v",
    ("ecmwf", "u"): "ecmwf-wind-u",
    ("ecmwf", "v"): "ecmwf-wind-v",
    ("ecmwf", "w"): "ecmwf-wind-vertical",
    ("ecmwf", "z"): "ecmwf-geopotential",
    ("ecmwf", "pcp"): "ecmwf-precipitation",
    ("ecmwf", "wind-intensity-10m"): "ecmwf-wind-intensity",
    ("ecmwf", "wind-intensity"): "ecmwf-wind-intensity",
    ("ecmwf", "wind-uv"): "ecmwf-wind-uv",
    ("ecmwf", "wind-uv-10m"): "ecmwf-wind-uv",

    ("ipma", "BUI"): "ipma-build-up-index",
    ("ipma", "DC"): "ipma-drought-code",
    ("ipma", "DMC"): "ipma-duff-moisture-code",
    ("ipma", "FFMC"): "ipma-fine-fuel-moisture-content",
    ("ipma", "FWI"): "ipma-fire-weather-index",
    ("ipma", "PERC_FWI"): "ipma-fire-weather-index-percentiles",
    ("ipma", "ISI"): "ipma-initial-spread-index",
    ("ipma", "RCM"): "ipma-rcm",

    ("lsasaf", "BUI"): "lsasaf-build-up-index",
    ("lsasaf", "DC"): "lsasaf-drought-code",
    ("lsasaf", "DMC"): "lsasaf-duff-moisture-code",
    ("lsasaf", "FFMC"): "lsasaf-fine-fuel-moisture-content",
    ("lsasaf", "FWI"): "lsasaf-fire-weather-index",
    ("lsasaf", "ISI"): "lsasaf-initial-spread-index",
    ("lsasaf", "Risk"): "lsasaf-fire-risk-map",
    ("lsasaf", "P2000"): "lsasaf-extreme-probabilities",
    ("lsasaf", "P2000a"): "lsasaf-extreme-probabilities-anomalies",
}


GranuleInfo = namedtuple(
    "GranuleInfo",
    [
        "band_index",
        "dimension_values"
    ]
)


class BaseGranulePublisher:
    dimensions = dict()

    def __init__(
            self,
            var_name: str,
            coverage_id: str,
            base_url: str,
            auth: typing.Optional[typing.Tuple[str, str]]
    ):
        self.var_name = var_name
        self.coverage_id = coverage_id
        self.base_url = base_url
        self.auth = auth

    def analyze_product(self, path: str):
        raise NotImplementedError

    def find_published_granule(
            self,
            dimension_values: typing.Dict
    ) -> typing.Optional[str]:
        filter_pattern = "filter[dimension_values__{}]"
        temporal_format = "%Y-%m-%dT%H:%M:%S"
        filter_params = {}
        for dim_name, dim_value in dimension_values.items():
            name = filter_pattern.format(dim_name)
            if isinstance(dim_value, dt.datetime):
                filter_params[name] = dim_value.strftime(temporal_format)
            else:
                filter_params[name] = str(dim_value)
        response = requests.get(
            f"{self.base_url}/coverages/{self.coverage_id}/granules/",
            headers={
                "Content-Type": CONTENT_TYPE,
            },
            params=filter_params
        )
        response.raise_for_status()
        data = response.json()["data"]
        return data[0]["id"] if len(data) > 0 else None

    def get_storage_details_payload(self, filename: str, band: int):
        return {
            "filename": filename,
            "band": band
        }

    def get_dimension_values_payload(self, dimension_values: typing.Dict):
        result = {}
        for dim_name, dim_value in dimension_values.items():
            if isinstance(dim_value, dt.datetime):
                result[dim_name] = dim_value.isoformat()
            else:
                result[dim_name] = str(dim_value)
        return result

    def publish_granule(
            self,
            filename: str,
            band_index: int,
            bbox: str,
            dimension_values: typing.Dict,
    ):
        response = requests.post(
            f"{self.base_url}/granules/",
            headers={
                "Content-Type": CONTENT_TYPE,
            },
            auth=self.auth,
            json={
                "data": {
                    "type": "Granule",
                    "attributes": {
                        "bbox_native": bbox,
                        "storage_details": self.get_storage_details_payload(
                            filename, band_index),
                        "dimension_values": self.get_dimension_values_payload(
                            dimension_values)
                    },
                    "relationships": {
                        "coverage": {
                            "data": {
                                "type": "Coverage",
                                "id": self.coverage_id
                            }
                        }
                    }
                }
            }
        )
        response.raise_for_status()
        return response.status_code

    def update_granule(
            self,
            granule_id: str,
            filename: str,
            band_index: int,
            bbox: str,
            dimension_values: typing.Dict,
    ):
        response = requests.patch(
            f"{self.base_url}/granules/{granule_id}/",
            headers={
                "Content-Type": CONTENT_TYPE,
            },
            auth=self.auth,
            json={
                "data": {
                    "id": granule_id,
                    "type": "Granule",
                    "attributes": {
                        "bbox_native": bbox,
                        "storage_details": self.get_storage_details_payload(
                            filename, band_index),
                        "dimension_values": self.get_dimension_values_payload(
                            dimension_values)
                    },
                }
            }
        )
        response.raise_for_status()
        return response.status_code


class NwpGranulePublisher(BaseGranulePublisher):

    def analyze_product(
            self, path: str
    ) -> typing.Tuple[str, typing.List[GranuleInfo]]:
        ds = gdal.Open(path)
        granule_infos = []
        bbox = get_bbox_wkt(ds)
        step_unit, raw_reference_time = ds.GetMetadataItem(
            "time#units").partition(" since ")[::2]
        if step_unit not in ("days", "hours"):
            raise RuntimeError(f"Invalid unit: {step_unit}")
        reference_time = dateutil.parser.parse(raw_reference_time)
        for band_index in range(1, ds.RasterCount + 1):
            band = ds.GetRasterBand(band_index)
            step = int(band.GetMetadataItem("NETCDF_DIM_step"))
            time = reference_time + dt.timedelta(**{step_unit: step})

            pressure_level = band.GetMetadataItem(
                f"NETCDF_DIM_{PRESSURE_LEVEL_NETCDF_DIMENSION_NAME}")
            if pressure_level is not None:
                pressure_level = f"{pressure_level}hPa"
            else:
                pressure_level = "SURFACE"
            granule_infos.append(
                GranuleInfo(
                    band_index=band_index,
                    dimension_values={
                        "reference_time": reference_time,
                        "time": time,
                        "pressure_level": pressure_level
                    }
                )
            )
        ds = None
        return bbox, granule_infos


class IpmaGranulePublisher(BaseGranulePublisher):

    def get_storage_details_payload(self, filename: str, band: int):
        payload = super().get_storage_details_payload(filename, band)
        payload.update(
            subdataset_prefix="NETCDF",
            subdataset_suffix=self.var_name
        )
        return payload

    def find_published_granule(
            self,
            dimension_values: typing.Dict
    ) -> typing.Optional[str]:
        dim_values = dimension_values.copy()
        dim_values.pop("reference_time")
        return super().find_published_granule(dim_values)

    def analyze_product(
            self,
            path: str
    ) -> typing.Tuple[str, typing.List[GranuleInfo]]:
        """
        Extract relevant information from IPMA based products.

        -  They have several subdatasets
        -  The reference time is not the time shown in the units of the temporal
           dimension. The temporal unit is relative to the time of the first
           temporal step, which is 24 hours after the reference time
        -  The temporal dimension is named `time`

        """
        main_ds = gdal.Open(path)
        for sub_dataset_path, sub_dataset_info in main_ds.GetSubDatasets():
            if sub_dataset_path.rpartition(":")[-1] == self.var_name:
                ds_path = sub_dataset_path
                break
        else:
            raise IOError(
                f"Could not find subdataset {self.var_name!r} on {path!r}")
        main_ds = None
        ds = gdal.Open(ds_path)
        bbox = get_bbox_wkt(ds)
        raw_first_step = ds.GetMetadataItem(
            "time#units").rpartition(" since ")[-1]
        first_step = dateutil.parser.parse(raw_first_step)
        reference_time = first_step - dt.timedelta(hours=24)
        granule_infos = []
        for band_index in range(1, ds.RasterCount + 1):
            band = ds.GetRasterBand(band_index)
            step = int(band.GetMetadataItem("NETCDF_DIM_time"))
            time = reference_time + dt.timedelta(hours=step)
            granule_infos.append(
                GranuleInfo(
                    band_index=band_index,
                    dimension_values={
                        "reference_time": reference_time,
                        "time": time,
                    }
                )
            )
        ds = None
        return bbox, granule_infos


class LsasafGranulePublisher(NwpGranulePublisher):

    def analyze_product(
            self, path: str
    ) -> typing.Tuple[str, typing.List[GranuleInfo]]:
        bbox, granule_infos = super().analyze_product(path)
        for info in granule_infos:
            del info.dimension_values["pressure_level"]
        return bbox, granule_infos


def get_granule_publisher_class(
        data_source: str) -> typing.Type[BaseGranulePublisher]:
    publisher_class = {
        "arome": NwpGranulePublisher,
        "ecmwf": NwpGranulePublisher,
        "ipma": IpmaGranulePublisher,
        "lsasaf": LsasafGranulePublisher,
    }[data_source]
    return publisher_class


def main(
        generated_product: packages.RetrievedMember,
        package: typing.Optional[packages.PythonCallbackPackage] = None,
        **kwargs
):
    try:
        var_name = package.callback_arguments["var_name"]
        data_source = package.callback_arguments["data_source"]
        coverage_name = COVERAGE_NAME_MAP[(data_source, var_name)]
    except KeyError:
        raise RuntimeError(
            "Invalid `var_name` or `data_source` callback arguments")
    base_url = conf.get("spatial_data_server", "url")
    coverage_details = get_coverage_details(base_url, coverage_name)
    coverage_id = coverage_details["id"]
    auth_user, auth_password = get_spatial_server_credentials()
    publisher_class = get_granule_publisher_class(data_source)
    publisher = publisher_class(
        var_name,
        coverage_id,
        base_url,
        auth=(auth_user, auth_password)
    )
    product_path = generated_product.local_path
    bbox, granule_infos = publisher.analyze_product(product_path)
    filename = product_path.rpartition("/")[-1]
    logger.debug(f"filename: {filename}")
    futures = {}
    # restrict the number of workers because we are sidestepping airflow as
    # the concurrency manager here - the machine may be under load from other
    # processes already
    with concurrent.futures.ThreadPoolExecutor(max_workers=3) as executor:
        for band_index, dimension_values in granule_infos:
            exec_args = (
                publisher,
                filename,
                band_index,
                bbox,
                dimension_values
            )
            fut = executor.submit(publish_granule, *exec_args)
            futures[fut] = exec_args
    for completed_future in concurrent.futures.as_completed(futures):
        result = completed_future.result()
        logger.debug(f"Future  has completed with result: {result}")
    logger.info("Refreshing coverage...")
    refresh_coverage(
        base_url,
        coverage_id,
        username=auth_user,
        password=auth_password
    )


def publish_granule(
        publisher: BaseGranulePublisher,
        filename: str,
        band_index: int,
        bbox: str,
        dimension_values: typing.Dict
):
    granule_id = publisher.find_published_granule(dimension_values)
    if granule_id is None:
        logger.debug("Publishing granule...")
        result = publisher.publish_granule(
            filename, band_index, bbox, dimension_values)
    else:
        logger.debug("Updating granule...")
        result = publisher.update_granule(
            granule_id, filename, band_index, bbox, dimension_values)
    return result


# def old_main(
#         generated_product: packages.RetrievedMember,
#         package: typing.Optional[packages.PythonCallbackPackage] = None,
#         **kwargs
# ):
#     try:
#         var_name = package.callback_arguments["var_name"]
#         data_source = package.callback_arguments["data_source"]
#         coverage_name = COVERAGE_NAME_MAP[(data_source, var_name)]
#     except KeyError:
#         raise RuntimeError(
#             "Invalid `var_name` or `data_source` callback arguments")
#     base_url = conf.get("spatial_data_server", "url")
#     logger.debug(f"base_url: {base_url}")
#     coverage = get_coverage_details(base_url, coverage_name)
#     coverage_id = coverage["id"]
#     logger.debug(f"coverage_id: {coverage_id}")
#     if data_source.lower() == "ipma":
#         reference_time, bbox, granule_infos = analyze_ipma_product(
#             generated_product.local_path, var_name)
#         subdataset_suffix = var_name
#     else:
#         reference_time, bbox, granule_infos = analyze_product(
#             generated_product.local_path)
#         subdataset_suffix = None
#     filename = generated_product.local_path.rpartition("/")[-1]
#     logger.debug(f"filename: {filename}")
#     logger.debug(f"reference_time: {reference_time}")
#     logger.debug(f"bbox: {bbox}")
#
#     futures = {}
#     # restrict the number of workers because we are sidestepping airflow as
#     # the concurrency manager here - the machine may be under load from other
#     # processes already
#     auth_user, auth_password = get_spatial_server_credentials()
#     with concurrent.futures.ThreadPoolExecutor(max_workers=3) as executor:
#         for band_index, time, pressure_level in granule_infos:
#             exec_args = (
#                 base_url,
#                 band_index,
#                 reference_time,
#                 time,
#                 coverage_id,
#                 filename,
#                 bbox,
#             )
#             exec_kwargs = {
#                 "pressure_level": pressure_level,
#                 "username": auth_user,
#                 "password": auth_password,
#                 "subdataset_suffix": subdataset_suffix,
#             }
#             fut = executor.submit(
#                 maybe_publish_granule, *exec_args, **exec_kwargs)
#             futures[fut] = (exec_args, exec_kwargs)
#     for completed_future in concurrent.futures.as_completed(futures):
#         result = completed_future.result()
#         logger.debug(f"Future  has completed with result: {result}")
#     logger.info("Refreshing coverage...")
#     refresh_coverage(
#         base_url, coverage_id,
#         username=auth_user,
#         password=auth_password
#     )


# def analyze_ipma_product(
#         path: str,
#         variable: str
# ) -> typing.Tuple[
#     dt.datetime,
#     str,
#     typing.List[typing.Tuple[int, dt.datetime, typing.Optional[str]]]
# ]:
#     """
#     Extract relevant information from IPMA based products.
#
#     IPMA products have some particularities that justify a different function
#     from `analyze_product`:
#
#     -  They have several subdatasets
#     -  The reference time is not the time shown in the units of the temporal
#        dimension. The temporal unit is relative to the time of the first
#        temporal step, which is 24 hours after the reference time
#     -  The temporal dimension is named `time`
#
#     """
#
#     main_ds = gdal.Open(path)
#     for sub_dataset_path, sub_dataset_info in main_ds.GetSubDatasets():
#         if sub_dataset_path.rpartition(":")[-1] == variable:
#             ds_path = sub_dataset_path
#             break
#     else:
#         raise IOError(f"Could not find subdataset {variable!r} on {path!r}")
#     main_ds = None
#     ds = gdal.Open(ds_path)
#     granule_infos = []
#     bbox = get_bbox_wkt(ds)
#     raw_first_step = ds.GetMetadataItem("time#units").rpartition(" since ")[-1]
#     first_step = dateutil.parser.parse(raw_first_step)
#     reference_time = first_step - dt.timedelta(hours=24)
#     for band_index in range(1, ds.RasterCount + 1):
#         band = ds.GetRasterBand(band_index)
#         step = int(band.GetMetadataItem("NETCDF_DIM_time"))
#         time = reference_time + dt.timedelta(hours=step)
#         pressure_level = band.GetMetadataItem(
#             f"NETCDF_DIM_{PRESSURE_LEVEL_NETCDF_DIMENSION_NAME}")
#         granule_infos.append((band_index, time, pressure_level))
#     ds = None
#     return reference_time, bbox, granule_infos


# def analyze_product(path: str):
#     ds = gdal.Open(path)
#     granule_infos = []
#     bbox = get_bbox_wkt(ds)
#     raw_reference_time = ds.GetMetadataItem(
#         "time#units").rpartition("since")[-1]
#     reference_time = dateutil.parser.parse(raw_reference_time)
#     for band_index in range(1, ds.RasterCount + 1):
#         band = ds.GetRasterBand(band_index)
#         step = int(band.GetMetadataItem("NETCDF_DIM_step"))
#         time = reference_time + dt.timedelta(hours=step)
#         pressure_level = band.GetMetadataItem(
#             f"NETCDF_DIM_{PRESSURE_LEVEL_NETCDF_DIMENSION_NAME}")
#         granule_infos.append((band_index, time, pressure_level))
#     ds = None
#     return reference_time, bbox, granule_infos


def get_coverage_details(base_url: str, name: str) -> typing.Dict:
    response = requests.get(
        f"{base_url}/coverages/",
        params={
            "filter[name]": name
        }
    )
    response.raise_for_status()
    try:
        coverage = response.json()["data"][0]
    except IndexError:
        raise RuntimeError(
            f"Could not find a coverage named {name} on the spatial data "
            f"server"
        )
    else:
        return coverage


# def maybe_publish_granule(
#         base_url: str,
#         band: int,
#         reference_time: pd.Timestamp,
#         time: pd.Timestamp,
#         coverage_id: str,
#         filename: str,
#         bbox: str,
#         pressure_level: typing.Optional[str] = None,
#         username: typing.Optional[str] = None,
#         password: typing.Optional[str] = None,
#         subdataset_suffix: typing.Optional[str] = None,
# ) -> typing.Optional[str]:
#     granule_id = get_published_id(
#         base_url, reference_time, time, coverage_id,
#         pressure_level=pressure_level
#     )
#     dimension_values_payload = get_dimension_values_payload(
#         time, reference_time, pressure_level)
#     auth = (username, password) if username else None
#     storage_details = {
#         "filename": filename,
#         "band": band,
#     }
#     if subdataset_suffix is not None:
#         storage_details.update(
#             subdataset_prefix="NETCDF",
#             subdataset_suffix=subdataset_suffix
#         )
#     common_args = (
#         base_url,
#         bbox,
#         dimension_values_payload,
#         auth,
#         storage_details,
#     )
#     if granule_id is not None:
#         logger.info(
#             f"Granule already published with id {granule_id!r}, updating...")
#         result = old_update_granule(granule_id, *common_args)
#     else:
#         logger.info("Publishing granule...")
#         result = old_publish_granule(coverage_id, *common_args)
#     return result


# def get_published_id(
#         base_url: str,
#         reference_time: pd.Timestamp,
#         time: pd.Timestamp,
#         coverage_id: str,
#         pressure_level: typing.Optional[str] = None
# ) -> typing.Optional[str]:
#     filter_pattern = "filter[dimension_values__{}]"
#     temporal_format = "%Y-%m-%dT%H:%M:%S"
#     filter_params = {
#         filter_pattern.format("time"): time.strftime(temporal_format),
#         filter_pattern.format("reference_time"): reference_time.strftime(
#             temporal_format),
#     }
#     if pressure_level is not None and pressure_level != "SURFACE":
#         filter_params[filter_pattern.format("pressure_level")] = (
#             f"{pressure_level}hPa")
#     else:
#         filter_params[filter_pattern.format("pressure_level")] = "SURFACE"
#     response = requests.get(
#         f"{base_url}/coverages/{coverage_id}/granules/",
#         headers={
#             "Content-Type": CONTENT_TYPE,
#         },
#         params=filter_params
#     )
#     response.raise_for_status()
#     data = response.json()["data"]
#     return data[0]["id"] if len(data) > 0 else None


# def get_dimension_values_payload(
#         time: dt.datetime,
#         reference_time: typing.Optional[dt.datetime] = None,
#         pressure_level: typing.Optional[str] = None
# ):
#     result = {
#         "time": time.isoformat(),
#     }
#     if reference_time is not None:
#         result["reference_time"] = reference_time.isoformat()
#     if pressure_level is not None:
#         result["pressure_level"] = f"{pressure_level}hPa"
#     else:
#         result["pressure_level"] = "SURFACE"
#     return result


# def old_update_granule(
#         granule_id: str,
#         base_url,
#         bbox,
#         dimension_values_payload: typing.Dict,
#         auth: typing.Optional[typing.Tuple[str, str]],
#         storage_details_payload: typing.Dict,
# ):
#     response = requests.patch(
#         f"{base_url}/granules/{granule_id}/",
#         headers={
#             "Content-Type": CONTENT_TYPE,
#         },
#         auth=auth,
#         json={
#             "data": {
#                 "id": granule_id,
#                 "type": "Granule",
#                 "attributes": {
#                     "bbox_native": bbox,
#                     "storage_details": storage_details_payload,
#                     "dimension_values": dimension_values_payload
#                 },
#             }
#         }
#     )
#     response.raise_for_status()
#     return response.status_code


# def old_publish_granule(
#         coverage_id: str,
#         base_url: str,
#         bbox: str,
#         dimension_values_payload: typing.Dict,
#         auth: typing.Optional[typing.Tuple[str, str]],
#         storage_details_payload: typing.Dict,
# ):
#     response = requests.post(
#         f"{base_url}/granules/",
#         headers={
#             "Content-Type": CONTENT_TYPE,
#         },
#         auth=auth,
#         json={
#             "data": {
#                 "type": "Granule",
#                 "attributes": {
#                     "bbox_native": bbox,
#                     "storage_details": storage_details_payload,
#                     "dimension_values": dimension_values_payload
#                 },
#                 "relationships": {
#                     "coverage": {
#                         "data": {
#                             "type": "Coverage",
#                             "id": coverage_id
#                         }
#                     }
#                 }
#             }
#         }
#     )
#     response.raise_for_status()
#     return response.status_code


def refresh_coverage(
        base_url: str,
        coverage_id: str,
        username: typing.Optional[str] = None,
        password: typing.Optional[str] = None,
):
    """Ask the spatial server to refresh the coverage

    Refreshing a coverage will cause all newly created granules to become
    available in the WMS service

    """
    refresh_time = dt.datetime.now(tz=dt.timezone.utc) + dt.timedelta(hours=1)
    auth = (username, password) if username else None
    response = requests.patch(
        f"{base_url}/coverages/{coverage_id}/",
        auth=auth,
        headers={
            "Content-Type": CONTENT_TYPE,
        },
        json={
            "data": {
                "id": coverage_id,
                "type": "Coverage",
                "attributes": {
                    "last_refreshed": refresh_time.isoformat()
                }
            }
        }
    )
    response.raise_for_status()
    content = response.json()
    return content["data"]


def get_bbox_wkt(ds) -> str:
    ulx, xres, xskew, uly, yskew, yres = ds.GetGeoTransform()
    cols = ds.RasterXSize
    rows = ds.RasterYSize
    llx = ulx + rows*xskew
    lly = uly + rows*yres
    lrx = ulx + cols*xres + rows*xskew
    lry = uly + cols*yskew + rows*yres
    urx = ulx + cols*xres
    ury = uly + cols*yskew
    return (
        f"POLYGON(("
        f"{llx} {lly}, "
        f"{lrx} {lry}, "
        f"{urx} {ury}, "
        f"{ulx} {uly}, "
        f"{llx} {lly}"
        f"))"
    )


def get_spatial_server_credentials() -> typing.Tuple[
    typing.Optional[str],
    typing.Optional[str]
]:
    # lets see if the creds are in env first, then check docker
    # secrets, then fallback to airflow conf
    name_variable = "AIRFLOW__SPATIAL_DATA_SERVER__USER_NAME"
    password_variable = "AIRFLOW__SPATIAL_DATA_SERVER__USER_PASSWORD"
    name = os.getenv(name_variable)
    password = os.getenv(password_variable)
    if name is None:
        name_file = Path(f"/run/secrets/{name_variable}")
        password_file = Path(f"/run/secrets/{password_variable}")
        if name_file.is_file():
            name = name_file.read_text().strip()
            password = password_file.read_text().strip()
        if name is None:
            name = conf.get("spatial_data_server", "user_name")
            password = conf.get("spatial_data_server", "user_password")
    return name, password
