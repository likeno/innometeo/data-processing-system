#MIT License
#
#Copyright (c) 2017 likeno
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.

"""soko callback functions for the configured packages."""

import bz2
import logging
import os
import shutil

logger = logging.getLogger(__name__)


def retrieve_inputs(*retrieved_inputs, **kwargs):
    # TODO: this could be done concurrently instead...
    for retrieved_member in retrieved_inputs:
        path = retrieved_member.local_path
        if path is None:
            logger.debug("{} was not retrieved, "
                         "skipping...".format(retrieved_member.member))
            continue
        else:
            _retrieve_input(
                path,
                output_dir=kwargs["output_dir"],
                decompress=kwargs.get("decompress", True)
            )

def _retrieve_input(local_path, output_dir, decompress=True):
    if local_path.endswith(".bz2") and decompress:
        output_path = os.path.join(
            output_dir,
            os.path.basename(local_path).replace(".bz2", "")
        )
        logger.debug("Decompressing {!r} to "
                     "{!r}...".format(local_path, output_path))
        with bz2.BZ2File(local_path) as source_fh, \
                open(output_path, "wb") as destination_fh:
            destination_fh.write(source_fh.read())
    else:
        shutil.move(local_path, output_dir)


