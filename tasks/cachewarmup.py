# MIT License
#
# Copyright (c) 2020 likeno
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Functions for warming up the cache of the featureinfoserver"""

import concurrent.futures
import datetime as dt
import logging
import typing

import requests
from airflow.configuration import conf
from soko import packages

logger = logging.getLogger(__name__)

LAYER_NAME_MAP = {
    ("arome", "10u"): "arome.wind_u",
    ("arome", "10v"): "arome.wind_v",
    ("arome", "2d"): "arome.dewpoint",
    ("arome", "t"): "arome.temperature",
    ("arome", "2t"): "arome.temperature",
    ("arome", "hcc"): "arome.high_cloud_cover",
    ("arome", "mcc"): "arome.medium_cloud_cover",
    ("arome", "lcc"): "arome.low_cloud_cover",
    ("arome", "tcc"): "arome.total_cloud_cover",
    ("arome", "msl"): "arome.pressure",
    ("arome", "r"): "arome.relative_humidity",
    ("arome", "tp"): "arome.precipitation",
    ("arome", "u"): "arome.wind_u",
    ("arome", "v"): "arome.wind_v",
    ("arome", "w"): "arome.vertical_velocity",
    ("arome", "z"): "arome.geopotential",
    ("arome", "wind-intensity"): "arome.wind_intensity",
    ("arome", "wind-direction"): "arome.wind_direction",

    ("ecmwf", "t"): "ecmwf.temperature",
    ("ecmwf", "2t"): "ecmwf.temperature",
    ("ecmwf", "msl"): "ecmwf.pressure",
    ("ecmwf", "r"): "ecmwf.relative_humidity",
    ("ecmwf", "10u"): "ecmwf.wind_u",
    ("ecmwf", "10v"): "ecmwf.wind_v",
    ("ecmwf", "u"): "ecmwf.wind_u",
    ("ecmwf", "v"): "ecmwf.wind_v",
    ("ecmwf", "2d"): "ecmwf.dewpoint",
    ("ecmwf", "cape"): "ecmwf.cape",
    ("ecmwf", "capes"): "ecmwf.capes",
    ("ecmwf", "z"): "ecmwf.geopotential",
    ("ecmwf", "pcp"): "ecmwf.precipitation",
    ("ecmwf", "wind-intensity"): "ecmwf.wind_intensity",
    ("ecmwf", "wind-direction"): "ecmwf.wind_direction",

    ("ipma", "BUI"): "ipma.bui",
    ("ipma", "DC"): "ipma.dc",
    ("ipma", "DMC"): "ipma.dmc",
    ("ipma", "FFMC"): "ipma.ffmc",
    ("ipma", "FWI"): "ipma.fwi",
    ("ipma", "PERC_FWI"): "ipma.fwi_perc",
    ("ipma", "ISI"): "ipma.isi",
    ("ipma", "RCM"): "ipma.rcm",

    ("lsasaf", "BUI"): "lsasaf.bui",
    ("lsasaf", "DC"): "lsasaf.dc",
    ("lsasaf", "DMC"): "lsasaf.dmc",
    ("lsasaf", "FFMC"): "lsasaf.ffmc",
    ("lsasaf", "FWI"): "lsasaf.fwi",
    ("lsasaf", "ISI"): "lsasaf.isi",
    ("lsasaf", "Risk"): "lsasaf.risk",
    ("lsasaf", "P2000"): "lsasaf.p2000",
    ("lsasaf", "P2000a"): "lsasaf.p2000a",

    # CONFIG MISSING IN FEATUREINFOSERVER
    # ("ecmwf", "blh"): "ecmwf.boundary_layer_height.atlantic",
    # ("ecmwf", "deg0l"): "ecmwf.deg0l.atlantic",
    # ("ecmwf", "hcct"): "ecmwf.convective_cloud_top_height.atlantic",
    # ("ecmwf", "lcc"): "ecmwf.low_cloud_cover.atlantic",
    # ("ecmwf", "mcc"): "ecmwf.medium_cloud_cover.atlantic",
    # ("ecmwf", "hcc"): "ecmwf.high_cloud_cover.atlantic",
    # ("ecmwf", "tcc"): "ecmwf.total_cloud_cover.atlantic",
    # ("ecmwf", "sst"): "ecmwf.sea_surface_temperature.atlantic",
    # ("ecmwf", "ssrd"): "ecmwf.surface_solar_radiation_downwards.atlantic",
    # ("ecmwf", "uvb"): "ecmwf.downward_uv_surface_radiation.atlantic",
    # ("ecmwf", "10fg"): "ecmwf.wind_gust_10meter.atlantic",
    # ("ecmwf", "10fg3"): "ecmwf.wind_gust3_10meter.atlantic",
    # ("ecmwf", "w"): "ecmwf.vertical_velocity.atlantic",
}


def main(
        package: typing.Optional[packages.PythonCallbackPackage] = None,
        *args,
        **kwargs
):
    reference_time = package.timeslot
    try:
        var_name = package.callback_arguments["var_name"]
        data_source = package.callback_arguments["data_source"]
        layer_name = LAYER_NAME_MAP[(data_source, var_name)]
    except KeyError:
        raise RuntimeError(
            "Invalid `var_name` or `data_source` callback arguments")
    logger.debug(f"layer_name: {layer_name}")
    locations = retrieve_locations(conf.get("malmo_backend", "url"))
    featureinfo_url = conf.get("feature_info_server", "url")
    submitted = {}
    with concurrent.futures.ThreadPoolExecutor(max_workers=3) as executor:
        for location_info in locations:
            exec_kwargs = {
                "latitude": location_info["latitude"],
                "longitude": location_info["longitude"],
                "reference_time": reference_time,
                "times": [
                    reference_time + dt.timedelta(hours=s) for s in range(72)],
                "layers": [layer_name]
            }
            logger.debug(f"exec_kwargs: {exec_kwargs}")
            fut = executor.submit(
                request_feature_info,
                featureinfo_url,
                **exec_kwargs
            )
            submitted[fut] = location_info["name"]
    results = []
    for completed_future in concurrent.futures.as_completed(submitted):
        request_status_code = completed_future.result()
        location = submitted[completed_future]
        logger.debug(
            f"Future for location: {location!r} has completed with "
            f"result: {request_status_code}"
        )
        results.append((location, request_status_code))
    if not all(results):
        errored_locations = [
            r[0] for r in results if r[1] != requests.codes.ok]
        raise RuntimeError(
            f"Could not warm up the cache for these "
            f"locations: {', '.join(errored_locations)}"
        )


def request_feature_info(
        featureinfo_server_base_url: str,
        layers: typing.List[str],
        latitude: float,
        longitude: float,
        reference_time: dt.datetime,
        times: typing.List[dt.datetime]
):
    response = requests.get(
        f"{featureinfo_server_base_url}/feature/query",
        params={
            "latitude": latitude,
            "longitude": longitude,
            "reference_time": reference_time.isoformat().replace("+00:00", "Z"),
            "time": (t.isoformat().replace("+00:00", "Z") for t in times),
            "layer": ",".join(layers),
        }
    )
    return response.status_code


def retrieve_locations(
        malmo_backend_base_url: str) -> typing.List[typing.Dict]:
    response = requests.get(f"{malmo_backend_base_url}/api/locations")
    response.raise_for_status()
    return response.json()
