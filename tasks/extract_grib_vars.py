# MIT License
#
# Copyright (c) 2017 likeno
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""soko callback functions for the configured packages."""

import logging
import os
import shutil
from urllib.parse import urlparse

from nc_juice import nc_juice

logger = logging.getLogger(__name__)

logging.root.setLevel(logging.DEBUG)


def retrieve_grib_input(*retrieved_grib_inputs, **kwargs):
    grib_input, = retrieved_grib_inputs
    if grib_input.local_path is not None:
        logger.debug("Fetched GRIB file file: {}".format(grib_input.local_path))
    else:
        raise ValueError("Missing GRIB file: {}"
                         "".format(grib_input))


def extract_data(*grib_inputs, **kwargs):
    package = kwargs["package"]
    grib_input, = grib_inputs
    logger.debug("Extracting GRIB data from input "
                 "file: {}".format(grib_input.local_path))
    output_path = os.path.join(
        kwargs['output_dir'], 
        os.path.basename(
            urlparse(package.outputs[0].search_names[0]).path
        )
    )
    nc_juice.process_grib_dataset(grib_input.local_path, output_path,
                                  **package.callback_arguments)
