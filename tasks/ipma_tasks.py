#MIT License
#
#Copyright (c) 2017 likeno
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in all
#copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#SOFTWARE.

"""soko callback functions for the configured packages."""

import bz2
import logging
import os
import shutil
from urllib.parse import urlparse
import xarray as xr
import numpy as np
import datetime as dt

import nc_juice

logger = logging.getLogger(__name__)


def get_basic(retrieved_inputs, kwargs):
    """Return retrieved input paths, output paths
    and soko package.
    """
    package = kwargs['package']
    input_paths = [retrieved_member.local_path
                   for retrieved_member in retrieved_inputs
                   if retrieved_member.local_path is not None]
    for path_idx, input_path in enumerate(input_paths):
        input_paths[path_idx] = _retrieve_input(
            input_path,
            working_dir=kwargs["output_dir"],
            decompress=kwargs.get("decompress", True)
        )
    output_paths = [
        os.path.join(
            kwargs['output_dir'],
            os.path.basename(urlparse(output_resource.search_names[0]).path)
        ) for output_resource in package.outputs
    ]
    return input_paths, output_paths, package


def merge_ipma_frm(*retrieved_inputs, **kwargs):

    (input_paths, (output_path,),
     package) = get_basic(retrieved_inputs, kwargs)

    if any(input_paths):
        nc_juice.process_nc_datasets(
            input_paths, output_path,
            **package.callback_arguments
        )
    else:
        logger.debug("Missing input files for: {}".format(
            [retrieved_member.member
             for retrieved_member in retrieved_inputs]
        ))




def _retrieve_input(local_path, working_dir,
                           decompress=True, copy=False):
    if local_path.endswith(".bz2") and decompress:
        input_path = os.path.join(
            working_dir,
            os.path.basename(local_path).replace(".bz2", "")
        )
        logger.debug("Decompressing {!r} to "
                     "{!r}...".format(local_path, input_path))
        with bz2.BZ2File(local_path) as source_fh, \
                open(input_path, "wb") as destination_fh:
            destination_fh.write(source_fh.read())
    elif copy:
        shutil.copy(local_path, working_dir)
        input_path = os.path.join(working_dir,
                                   os.path.basename(local_path))
    else:
        input_path = local_path
    return input_path


