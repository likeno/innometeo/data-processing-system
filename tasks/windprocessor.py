# MIT License
#
# Copyright (c) 2020 likeno
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Functions for computing wind-related products"""

import logging
import os
import typing
from urllib.parse import urlparse

import numpy as np
from soko import packages
import xarray as xr

logger = logging.getLogger(__name__)


def generate_combined_wind(
        *retrieved_products: packages.RetrievedMember,
        package: typing.Optional[packages.PythonCallbackPackage] = None,
        output_dir: str = None,
        **kwargs
):
    """Create a single NetCDF file with the `u` and `v` variables.

    This function transforms 2 netCDF files (one with `u` and one with `v`)
    into a single netCDF with a single `components` variable, where the u
    and v values are stacked together. This new file has the extra `uv`
    dimension, which allows filtering only for u or only for v values.

    """

    u_component, v_component = sort_inputs(*retrieved_products)
    u_dataset = xr.open_dataset(u_component.local_path)
    v_dataset = xr.open_dataset(v_component.local_path)
    combined = xr.combine_by_coords([u_dataset, v_dataset])
    grid_mapping_dim_name = "mercator"
    stacking_dim_name = "uv"

    # remove the grid_mapping variable in order to be able to properly stack
    # the u and v variables into a single one - otherwise the grid_mapping
    # variable would also be stacked
    combined_no_grid_mapping = combined.drop(grid_mapping_dim_name)
    stacked = combined_no_grid_mapping.to_array(dim=stacking_dim_name)

    # for some reason GDAL will not read the coordinate values if they are
    # strings - therefore we turn them into numbers and further down in the
    # code we use CF-Conventions `flag_values` and `flag_meanings` attributes
    # to preserve the information as attributes
    stacked["uv"] = np.where(stacked.uv == "u", 1, 2)

    var_name = "components"
    stacked_ds = stacked.to_dataset(name=var_name)

    # insert the grid_mapping variable back into the stacked dataset
    stacked_ds[grid_mapping_dim_name] = combined[grid_mapping_dim_name]

    # finally, adjust the attributes of the main variable and the uv dimension
    stacked_ds.data_vars[var_name].attrs.update({
        "long_name": "Stacked u and v wind components",
        "grid_mapping": grid_mapping_dim_name,
        "units": "m s**-1",
    })
    stacked_ds[stacking_dim_name].attrs.update({
        "long_name": "Wind component",
        "flag_values": [1, 2],
        "flag_meanings": ["u", "v"]
    })
    output_path = _prepare_output_path(package, output_dir)
    stacked_ds.to_netcdf(output_path)


def generate_wind_direction(
        *retrieved_products: packages.RetrievedMember,
        package: typing.Optional[packages.PythonCallbackPackage] = None,
        output_dir: str = None,
        **kwargs
):
    """Create a NetCDF file with wind direction.

    Wind direction is expressed in radians. It is calculated as the arctan2
    of the `v` and `u` wind components.

    """

    u_component, v_component = sort_inputs(*retrieved_products)
    output_path = _prepare_output_path(package, output_dir)
    u_dataset = xr.open_dataset(u_component.local_path)
    u_var_name = get_wind_component_variable_name(u_dataset)
    v_dataset = xr.open_dataset(v_component.local_path)
    v_var_name = get_wind_component_variable_name(v_dataset)
    directions = np.arctan2(v_dataset[v_var_name], u_dataset[u_var_name])
    directions.attrs.update({
        "long_name": "wind direction",
        "units": "rad",
        "grid_mapping": "mercator"
    })
    output_dataset = directions.to_dataset(name="direction")
    output_dataset["mercator"] = u_dataset["mercator"]
    output_dataset.to_netcdf(output_path)
    return output_path


def generate_wind_intensity(
        *retrieved_products: packages.RetrievedMember,
        package: typing.Optional[packages.PythonCallbackPackage] = None,
        output_dir: str = None,
        **kwargs
):
    """Create a NetCDF file with wind intensity.

    Wind intensity is calculated as the magnitude (or modulus) of the vector
    (u, v).

    """

    u_component, v_component = sort_inputs(*retrieved_products)
    output_path = _prepare_output_path(package, output_dir)
    u_dataset = xr.open_dataset(u_component.local_path)
    u_var_name = get_wind_component_variable_name(u_dataset)
    v_dataset = xr.open_dataset(v_component.local_path)
    v_var_name = get_wind_component_variable_name(v_dataset)
    intensities = np.sqrt(
        u_dataset[u_var_name] ** 2 + v_dataset[v_var_name] ** 2)
    intensities.attrs.update({
        "long_name": "Wind intensity",
        "units": "m s**-1",
        "grid_mapping": "mercator"
    })
    output_dataset = intensities.to_dataset(name="intensity")
    output_dataset["mercator"] = u_dataset["mercator"]
    output_dataset.to_netcdf(output_path)
    return output_path


def sort_inputs(*inputs: packages.RetrievedMember):
    u_component = [i for i in inputs if "wind_u" in i.member.resource.name][0]
    v_component = [i for i in inputs if "wind_v" in i.member.resource.name][0]
    return u_component, v_component


def get_wind_component_variable_name(dataset: xr.Dataset):
    for name in dataset.data_vars.keys():
        if name.startswith("u") or name.startswith("v"):
            return name


def _prepare_output_path(package, output_dir):
    output_path = os.path.join(
        output_dir,
        os.path.basename(
            urlparse(package.outputs[0].search_names[0]).path
        )
    )
    output_dir = os.path.dirname(output_path)
    logger.debug(f"output_path: {output_path}")
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)
    return output_path
