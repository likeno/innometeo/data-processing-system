import logging
import os
import shlex
import subprocess
import typing
from urllib.parse import urlparse

from airflow.configuration import conf
from soko import packages

logger = logging.getLogger(__name__)

def main(
        grib_input: packages.RetrievedMember,
        **kwargs
):
    try:
        short_name = kwargs["short_name"]
        package = kwargs["package"]
        output_dir = kwargs["output_dir"]
    except KeyError:
        raise RuntimeError(
            "Invalid task settings, missing one or more arguments")

    output_path = os.path.join(
        output_dir,
        os.path.basename(urlparse(package.outputs[0].search_names[0]).path)
    )

    cmd = f"grib_copy -w shortName={short_name} {grib_input.local_path} {output_path}"
    logger.debug("Will execute {}".format(cmd))
    status = subprocess.run(shlex.split(cmd))

    if status.returncode < 0:
        raise RuntimeError(
            "Failed to split grib data. Task exited with status code {}".format(status.returncode)
        )
    else:
        logger.info("Finished processing grib data for variable {}".format(short_name))
