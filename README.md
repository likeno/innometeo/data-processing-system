# data-processing-system

## Deployment

Generate the following files:

- `docker/secrets/airflow-admin-password`
- `docker/secrets/airflow-celery-broker-url`
- `docker/secrets/airflow-celery-result-backend`
- `docker/secrets/airflow-core-sqlalchemy-conn`
- `docker/secrets/airflow-webserver-secret-key`
- `docker/secrets/db-password`
- `docker/secrets/netrc`
- `docker/secrets/ssh-config`
- `docker/secrets/ssh-known-hosts`
- `docker/secrets/ssh-private-key`
- `docker/secrets/ssh-public-key`


Example:

```
mkdir docker/secrets
printf "dummy" > docker/secrets/airflow-admin-password
printf "amqp://guest:guest@rabbit:5672//" > docker/secrets/airflow-celery-broker-url
printf "redis://redis:6379/0" > docker/secrets/airflow-celery-result-backend
printf "postgresql://dummyuser:dummy@db/dummydb" > docker/secrets/airflow-core-sqlalchemy-conn
printf "something" > docker/secrets/airflow-webserver-secret-key
printf "dummy" > docker/secrets/db-password
printf "# dummy" > docker/secrets/netrc
printf "# dummy" > docker/secrets/ssh-config
printf "# dummy" > docker/secrets/ssh-known-hosts
printf "# dummy" > docker/secrets/ssh-private-key
printf "# dummy" > docker/secrets/ssh-public-key
```

Now deploy the docker stack file

```
cd docker && bash deploy.sh

# alternatively, pass `production` in order to use a production deployment
cd docker && bash deploy.sh production
```
