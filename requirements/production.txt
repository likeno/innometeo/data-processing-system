#
# NOTES:
# -  Keep this list sorted alphabetically, but put git URLS in the end
# -  nc_juice is not included here, even though it is a dependency, because it
#    is only installed after these. This is done in order to simplify nc_juice
#    installation. This way it is able to leverage the already installed
#    numpy, xarray, etc, which are pulled as wheels. Otherwise they'd have to
#    be built from source
#

apache-airflow==1.10.2
celery >=4.1.1, <4.2.0
cfgrib
cryptography==2.8
dask
flask-bcrypt>=0.7.1
gdal
h5py==2.8.0
Jinja2 <=2.10.0, >=2.7.3  # required by airflow
netCDF4
numpy
Pillow
psycopg2-binary>=2.7.4
redis
scipy
toolz
tzlocal <2.0.0.0, >=1.5.0.0  # required by airflow's pendulum version
xarray==0.13.0  # seems to be required in order to work with the numpy version pulled by airflow

git+https://gitlab.com/joao.macedo/ginjinha.git
git+https://gitlab.com/joao.macedo/msg_hdf5_to_netcdf.git
git+https://gitlab.com/likeno/soko.git@v0.14.1#egg=soko
