# MIT License
#
# Copyright (c) 2017 likeno
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Utility functions for DAGs."""

import datetime as dt
from getpass import getuser
import logging
import os

from airflow import AirflowException
from airflow import configuration

logger = logging.getLogger(__name__)


def get_default_args():

    default_args = {
        "owner": getuser(),
        "depends_on_past": False,
        "start_date": dt.datetime(2017, 1, 1, 0, 0),
        "provide_context": True,
        "temporal_slot_context_key": "next_execution_date",
    }
    try:
        # If airflow is configured with proper email settings, send e-mails on
        # task failure to the designated mailboxes, which are specified via env
        configuration.get("smtp", "smtp_user")
        default_args.update({
            "email": os.getenv("TASK_FAILURE_MAILING_LIST", "").split(),
            "email_on_retry": False,
            "email_on_failure": True
        })
    except AirflowException:
        pass
    return default_args


def fix_logging():
    """Workaround for airflow logging bugs"""
    logging.root.handlers = []
    log_level = configuration.get("core", "logging_level")
    logging.basicConfig(level=log_level.upper())
