from airflow import DAG
from airflow.models import Pool
from airflow import settings
from datetime import timedelta
from importlib import import_module
import sqlalchemy
import logging
import os

from soko.utilities import lazy_import, generate_context
from ginjinha import ginjinha_loader

logger = logging.getLogger(__name__)


def get_settings():
    """Load configuration settings"""
    settings_path = settings.conf.get('soko','settings_path')
    return ginjinha_loader.load_settings(settings_path)


def create_dags():
    """Create dags"""

    raw_settings = get_settings()

    for dag_id, dag_settings in raw_settings['dags'].items():
        logger.debug('Create DAG ({}).'.format(dag_id))
        dag = DAG(dag_id, **dag_settings['dag_args'])
        globals()[dag_id] = dag
        # Create tasks:
        for task_id, task_settings in dag_settings['tasks'].items():
            task = _create_task(task_id, dag, task_settings)
            globals()["{}_{}".format(dag_id, task_id)] = task

    # Create relationships (task triggers):
    for dag_id, dag_settings in raw_settings['dags'].items():
        for task_id, task_settings in dag_settings['tasks'].items():
            relationships = task_settings.get('relationships')
            if relationships is not None:

                logger.debug(f'Setting Task ({task_id}) relationships: {relationships}')
                upstream = relationships.get('upstream')
                if upstream is not None:
                    for up_task_id in upstream:
                        (globals()["{}_{}".format(dag_id, task_id)] <<
                         globals()["{}_{}".format(dag_id, up_task_id)])
                    
                downstream = relationships.get('downstream')
                if downstream is not None:
                    for down_task_id in downstream:
                        (globals()["{}_{}".format(dag_id, task_id)] >>
                         globals()["{}_{}".format(dag_id, down_task_id)])
    
    # Create airflow pools:
    pools = raw_settings.get('pools')
    if pools is not None:
        session = settings.Session()
        for p_name, (p_slots, p_description) in pools.items():
            _create_pool(p_name, p_slots, p_description, session)


def _create_task(task_id, dag, task_settings):
    logger.debug(f'Create Task ({task_id}).')

    task_args = task_settings.get('task_args')
    if task_args is not None:
        for arg_name, arg_val in task_args.items():

           if arg_name in ['delta', 'retry_delay']:
               task_settings['task_args'][arg_name] = (
                   timedelta(**eval("dict({})".format(arg_val)))
               )

           if arg_name == "python_callable":
               task_settings['task_args'][arg_name] = (
                   lazy_import(
                       task_settings['task_args'][arg_name])
               )

           if arg_name == "soko_settings":
               soko_context = generate_context()
               task_settings['task_args'][arg_name].setdefault(
                   'context', {}
               ).update(soko_context)

    operator_str = task_settings.get(
        'operator', 'airflow.operators.python_operator.PythonOperator')
    operator = lazy_import(operator_str)
    
    return operator(task_id=task_id, dag=dag,
                    **task_settings['task_args'])


def _create_pool(name, slots, description, session):
    """Create a pool with a given parameters."""
    if not (name and name.strip()):
        raise ValueError("Pool name shouldn't be empty")

    try:
        slots = int(slots)
    except ValueError:
        raise ValueError("Bad value for `slots`: %s" % slots)

    session.expire_on_commit = False
    try:
        pool = session.query(Pool).filter_by(pool=name).first()
    except sqlalchemy.exc.ProgrammingError:
        pool = None

    if pool is None:
        logger.debug(f'Create pool ({name}).')
        pool = Pool(pool=name, slots=slots,
                    description=description)
        session.add(pool)
    else:
        logger.debug(f'Pool ({name}) already exists...')
    session.commit()


create_dags()

