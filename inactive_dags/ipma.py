# MIT License
#
# Copyright (c) 2017 likeno
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Airflow DAGs for processing AROME products and areas"""

import datetime as dt

from airflow import DAG
from airflow.operators import SokoOperator
from airflow.operators.sensors import TimeDeltaSensor

from __init__ import __version__
from utilities import get_default_args


dag = DAG(
    "ipma",
    description="v{} - Retrieve IPMA products".format(__version__),
    schedule_interval="30 23 * * *", # run every day at 23h30
    default_args=get_default_args(),
    params={
        "dag_version": __version__,
    }
)

retrieve_bui_input = SokoOperator(
    soko_package='retrieve_ipma_bui',
    task_id='retrieve_ipma_bui',
    dag=dag,
    retries=12*6,
    retry_delay=dt.timedelta(minutes=5),
    soko_execution_kwargs={'force_retrieve': True},
    pool='medium'
)

retrieve_dc_input = SokoOperator(
    soko_package='retrieve_ipma_dc',
    task_id='retrieve_ipma_dc',
    dag=dag,
    retries=12*6,
    retry_delay=dt.timedelta(minutes=5),
    soko_execution_kwargs={'force_retrieve': True},
    pool='medium'
)

retrieve_dmc_input = SokoOperator(
    soko_package='retrieve_ipma_dmc',
    task_id='retrieve_ipma_dmc',
    dag=dag,
    retries=12*6,
    retry_delay=dt.timedelta(minutes=5),
    soko_execution_kwargs={'force_retrieve': True},
    pool='medium'
)

retrieve_ffmc_input = SokoOperator(
    soko_package='retrieve_ipma_ffmc',
    task_id='retrieve_ipma_ffmc',
    dag=dag,
    retries=12*6,
    retry_delay=dt.timedelta(minutes=5),
    soko_execution_kwargs={'force_retrieve': True},
    pool='medium'
)

retrieve_fwi_input = SokoOperator(
    soko_package='retrieve_ipma_fwi',
    task_id='retrieve_ipma_fwi',
    dag=dag,
    retries=12*6,
    retry_delay=dt.timedelta(minutes=5),
    soko_execution_kwargs={'force_retrieve': True},
    pool='medium'
)

retrieve_isi_input = SokoOperator(
    soko_package='retrieve_ipma_isi',
    task_id='retrieve_ipma_isi',
    dag=dag,
    retries=12*6,
    retry_delay=dt.timedelta(minutes=5),
    soko_execution_kwargs={'force_retrieve': True},
    pool='medium'
)

retrieve_rcm_input = SokoOperator(
    soko_package='retrieve_ipma_rcm',
    task_id='retrieve_ipma_rcm',
    dag=dag,
    retries=12*6,
    retry_delay=dt.timedelta(minutes=5),
    soko_execution_kwargs={'force_retrieve': True},
    pool='medium'
)

retrieve_perc_input = SokoOperator(
    soko_package='retrieve_ipma_perc',
    task_id='retrieve_ipma_perc',
    dag=dag,
    retries=12*6,
    retry_delay=dt.timedelta(minutes=5),
    soko_execution_kwargs={'force_retrieve': True},
    pool='medium'
)

merge_ipma_frm = SokoOperator(
    soko_package='merge_ipma_frm',
    task_id='merge_ipma_frm',
    dag=dag,
    pool='medium'
)

merge_ipma_frm.set_upstream(retrieve_perc_input)
merge_ipma_frm.set_upstream(retrieve_rcm_input)
merge_ipma_frm.set_upstream(retrieve_isi_input)
merge_ipma_frm.set_upstream(retrieve_fwi_input)
merge_ipma_frm.set_upstream(retrieve_ffmc_input)
merge_ipma_frm.set_upstream(retrieve_dmc_input)
merge_ipma_frm.set_upstream(retrieve_dc_input)
merge_ipma_frm.set_upstream(retrieve_bui_input)
