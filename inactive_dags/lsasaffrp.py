# MIT License
#
# Copyright (c) 2017 likeno
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Airflow DAG for processing LSASAF's FRP product."""

import datetime as dt
from getpass import getuser

from airflow import DAG
from airflow.operators import SokoOperator

from __init__ import __version__
from utilities import get_default_args

dag = DAG(
    "lsasaf_frp",
    description="v{} - Retrieve LSASAF FRP product".format(__version__),
    schedule_interval="0,15,30,45 * * * *",  # run every 15 minutes
    default_args=get_default_args(),
    params={
        "dag_version": __version__,
    }
)

retrieve_frp_product = SokoOperator(
    soko_package="retrieve_frp_input",
    soko_package_context={},
    task_id="retrieve_frp_product",
    dag=dag,
    retries=11,
    retry_delay=dt.timedelta(minutes=5),
    soko_execution_kwargs={"force_retrieve":True},
    pool="medium",
)

extract_data = SokoOperator(
    soko_package="extract_frp_data",
    soko_package_context={},
    task_id="extract_frp_data",
    dag=dag,
    pool="high",
)
extract_data.set_upstream(retrieve_frp_product)
