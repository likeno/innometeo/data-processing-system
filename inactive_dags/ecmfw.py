# MIT License
#
# Copyright (c) 2017 likeno
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Airflow DAGs for processing ECMWF products"""

from collections import namedtuple
import datetime as dt

from airflow import DAG
from airflow.operators import SokoOperator
from airflow.operators.sensors import TimeDeltaSensor

from __init__ import __version__
from utilities import get_default_args

EcmwfArea = namedtuple("EcmwfArea", [
    "name",
    "short_name",
])


def generate_dag(defaults):
    area = defaults["soko_package_context"]["area"]
    dag = DAG(
        "ecmwf_{}".format(area.name),
        description="v{} - Retrieve ECMWF products for {}".format(
            __version__, area.name),
        schedule_interval="0 0,12 * * *",  # run twice a day, at 00h and 12h
        default_args=defaults,
        params={
            "dag_version": __version__,
        }
    )
    delay_execution = TimeDeltaSensor(
        delta=dt.timedelta(hours=7),
        poke_interval=60*5,  # try every 5 minutes
        task_id="delay_execution",
        dag=dag,
        pool="low",
    )
    retrieve_input = SokoOperator(
        soko_package="retrieve_ecmwf_input",
        task_id="retrieve_input",
        dag=dag,
        retries=12*6,  # accept inputs with 6 hours delay
        retry_delay=dt.timedelta(minutes=5),
        soko_execution_kwargs={"force_retrieve":True},
        pool="medium",
    )
    extract_temperature = SokoOperator(
        soko_package="extract_ecmwf_temperature",
        task_id="extract_temperature",
        dag=dag,
        pool="high",
    )
    extract_temperature2 = SokoOperator(
        soko_package="extract_ecmwf_temperature2",
        task_id="extract_temperature2",
        dag=dag,
        pool="high",
    )

    retrieve_input.set_upstream(delay_execution)
    extract_temperature.set_upstream(retrieve_input)
    extract_temperature2.set_upstream(retrieve_input)
    return dag


AREAS = [
    EcmwfArea(name="atlantic_pt", short_name="ATP"),
]

for ecmwf_area in AREAS:
    default_args = get_default_args()
    default_args["soko_package_context"] = {
        "area": ecmwf_area
    }
    ecmwf_dag = generate_dag(default_args)
    globals()[ecmwf_dag.dag_id] = ecmwf_dag
