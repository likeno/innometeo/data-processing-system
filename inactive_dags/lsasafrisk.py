# MIT License
#
# Copyright (c) 2017 likeno
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Airflow DAG for processing LSASAF's FWI product"""

import datetime as dt
from getpass import getuser

from airflow import DAG
from airflow.operators import SokoOperator

from __init__ import __version__
from utilities import get_default_args

dag = DAG(
    "lsasaf_risk",
    description="v{} - Retrieve LSASAF FWI Risk products".format(__version__),
    schedule_interval="0 22 * * *",  # run everyday at 22:00
    default_args=get_default_args(),
    params={
        "dag_version": __version__,
    }
)
dag.doc_md = (
    "Retrieve and prepare the LSASAF FWI Risk dataset. This product is "
    "distributed by LSASAF as a dataset of the FRM product."
)

retrieve_risk_products = SokoOperator(
    soko_package="retrieve_risk_inputs",
    task_id="retrieve_risk_products",
    dag=dag,
    retries=11,
    retry_delay=dt.timedelta(minutes=5),
    soko_execution_kwargs={"force_retrieve": True},
    pool="medium",
)
extract_risk = SokoOperator(
    soko_package="extract_risk",
    soko_package_context={},
    task_id="extract_risk",
    dag=dag,
    pool="high",
)
extract_p2000 = SokoOperator(
    soko_package="extract_p2000",
    soko_package_context={},
    task_id="extract_p2000",
    dag=dag,
    pool="high",
)
extract_p2000a = SokoOperator(
    soko_package="extract_p2000a",
    soko_package_context={},
    task_id="extract_p2000a",
    dag=dag,
    pool="high",
)

extract_risk.set_upstream(retrieve_risk_products)
extract_p2000.set_upstream(retrieve_risk_products)
extract_p2000a.set_upstream(retrieve_risk_products)
