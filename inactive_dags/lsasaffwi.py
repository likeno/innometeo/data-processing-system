# MIT License
#
# Copyright (c) 2017 likeno
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Airflow DAG for processing LSASAF's FWI product"""

import datetime as dt
from getpass import getuser

from airflow import DAG
from airflow.operators import SokoOperator

from __init__ import __version__
from utilities import get_default_args

dag = DAG(
    "lsasaf_fwi",
    description="v{} - Retrieve LSASAF FWI product".format(__version__),
    schedule_interval="0 22 * * *",  # run everyday at 22:00
    default_args=get_default_args(),
    params={
        "dag_version": __version__,
    }
)
dag.doc_md = (
    "Retrieve and prepare the LSASAF FWI dataset. This product is "
    "distributed by LSASAF as a dataset of the FRM product."
)
retrieve_frm_products = SokoOperator(
    soko_package="retrieve_frm_inputs",
    task_id="retrieve_frm_products",
    dag=dag,
    retries=11,
    retry_delay=dt.timedelta(minutes=5),
    soko_execution_kwargs={"force_retrieve":True},
    pool="medium",
)
extract_fwi = SokoOperator(
    soko_package="extract_fwi",
    soko_package_context={},
    task_id="extract_fwi",
    dag=dag,
    pool="high",
)
extract_bui = SokoOperator(
    soko_package="extract_bui",
    soko_package_context={},
    task_id="extract_bui",
    dag=dag,
    pool="high",
)
extract_dc = SokoOperator(
    soko_package="extract_dc",
    soko_package_context={},
    task_id="extract_dc",
    dag=dag,
    pool="high",
)
extract_dmc = SokoOperator(
    soko_package="extract_dmc",
    soko_package_context={},
    task_id="extract_dmc",
    dag=dag,
    pool="high",
)
extract_ffmc = SokoOperator(
    soko_package="extract_ffmc",
    soko_package_context={},
    task_id="extract_ffmc",
    dag=dag,
    pool="high",
)
extract_isi = SokoOperator(
    soko_package="extract_isi",
    soko_package_context={},
    task_id="extract_isi",
    dag=dag,
    pool="high",
)
extract_fwi.set_upstream(retrieve_frm_products)
extract_bui.set_upstream(retrieve_frm_products)
extract_dc.set_upstream(retrieve_frm_products)
extract_dmc.set_upstream(retrieve_frm_products)
extract_ffmc.set_upstream(retrieve_frm_products)
extract_isi.set_upstream(retrieve_frm_products)
