# MIT License
#
# Copyright (c) 2017 likeno
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Airflow DAGs for processing AROME products and areas"""

from collections import namedtuple
import datetime as dt

from airflow import DAG
from airflow.operators import SokoOperator
from airflow.operators.sensors import TimeDeltaSensor

from __init__ import __version__
from utilities import get_default_args

AromeArea = namedtuple("AromeArea", [
    "name",
    "short_name",
])


def generate_dag(defaults):
    area = defaults["soko_package_context"]["area"]
    dag = DAG(
        "arome_{}".format(area.name),
        description="v{} - Retrieve AROME products for {}".format(
            __version__, area.name),
        schedule_interval="0 0,12 * * *",  # run twice a day, at 00h and 12h
        default_args=defaults,
        params={
            "dag_version": __version__,
        }
    )
    delay_execution = TimeDeltaSensor(
        delta=dt.timedelta(hours=9),
        poke_interval=60*5,  # try every 5 minutes
        task_id="delay_execution",
        dag=dag,
        pool="low",
    )
    retrieve_input = SokoOperator(
        soko_package="retrieve_arome_input",
        task_id="retrieve_input",
        dag=dag,
        retries=12*6,  # accept inputs with 6 hours delay
        retry_delay=dt.timedelta(minutes=5),
        soko_execution_kwargs={"force_retrieve":True},
        pool="medium",
    )
    extract_wind_tracers_webgl = SokoOperator(
        soko_package="extract_arome_wind10_tracers_webgl",
        task_id="extract_wind_tracers_webgl",
        dag=dag,
        pool="high",
    )
    extract_wind_tracers = SokoOperator(
        soko_package="extract_arome_wind10_tracers",
        task_id="extract_wind_tracers",
        dag=dag,
        pool="high",
    )
    extract_temperature = SokoOperator(
        soko_package="extract_arome_temperature",
        task_id="extract_temperature",
        dag=dag,
        pool="high",
    )
    extract_relative_humidity = SokoOperator(
        soko_package="extract_arome_relative_humidity",
        task_id="extract_relative_humidity",
        dag=dag,
        pool="high",
    )
    extract_pressure = SokoOperator(
        soko_package="extract_arome_pressure",
        task_id="extract_pressure",
        dag=dag,
        pool="high",
    )
    extract_precipitation = SokoOperator(
        soko_package="extract_arome_precipitation",
        task_id="extract_precipitation",
        dag=dag,
        pool="high",
    )
    extract_wind = SokoOperator(
        soko_package="extract_arome_wind",
        task_id="extract_wind",
        dag=dag,
        pool="high",
    )

    extract_wind_module = SokoOperator(
        soko_package="extract_arome_wind_module",
        task_id="extract_wind_module",
        dag=dag,
        pool="high",
    )
    extract_wind_components = SokoOperator(
        soko_package="extract_arome_wind_components",
        task_id="extract_wind_components",
        dag=dag,
        pool="high",
    )
    extract_gust_module = SokoOperator(
        soko_package="extract_arome_gust_module",
        task_id="extract_gust_module",
        dag=dag,
        pool="high",
    )

    retrieve_input.set_upstream(delay_execution)
    extract_wind_tracers_webgl.set_upstream(retrieve_input)
    extract_wind_tracers.set_upstream(retrieve_input)
    extract_temperature.set_upstream(retrieve_input)

    if area.name == 'continent':
        extract_dewpoint = SokoOperator(
            soko_package="extract_arome_dewpoint",
            task_id="extract_dewpoint",
            dag=dag,
            pool="high",
        )
        extract_dewpoint.set_upstream(retrieve_input)

    extract_relative_humidity.set_upstream(retrieve_input)
    extract_pressure.set_upstream(retrieve_input)
    extract_precipitation.set_upstream(retrieve_input)
    extract_wind.set_upstream(retrieve_input)
    extract_wind_module.set_upstream(retrieve_input)
    extract_wind_components.set_upstream(retrieve_input)
    extract_gust_module.set_upstream(retrieve_input)
    return dag


AREAS = [
    AromeArea(name="continent", short_name="PT2"),
    AromeArea(name="azores", short_name="AZO"),
    AromeArea(name="madeira", short_name="MAD"),
]

for arome_area in AREAS:
    default_args = get_default_args()
    default_args["soko_package_context"] = {
        "area": arome_area
    }
    arome_dag = generate_dag(default_args)
    globals()[arome_dag.dag_id] = arome_dag
