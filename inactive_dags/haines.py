# MIT License
#
# Copyright (c) 2017 likeno
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Airflow DAGs for processing AROME products and areas"""

import datetime as dt

from airflow import DAG
from airflow.operators import SokoOperator
from airflow.operators.sensors import TimeDeltaSensor

from __init__ import __version__
from utilities import get_default_args


dag = DAG(
    "haines",
    description="v{} - Retrieve HAINES products".format(__version__),
    schedule_interval="0 16 * * *", # run every day at 16h
    default_args=get_default_args(),
    params={
        "dag_version": __version__,
    }
)
retrieve_haines_input = SokoOperator(
    soko_package="retrieve_haines_input",
    task_id="retrieve_haines_input",
    dag=dag,
    retries=12*6,  # accept inputs with 6 hours delay
    retry_delay=dt.timedelta(minutes=5),
    soko_execution_kwargs={"force_retrieve":True},
    pool="medium",
)
extract_haines_multilayer = SokoOperator(
    soko_package="extract_haines_multilayer",
    task_id="extract_haines_multilayer",
    dag=dag,
    pool="high",
)

extract_haines_multilayer.set_upstream(retrieve_haines_input)
